﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using System.Web.Http.Cors;
using RC_API.Models;
using System.Dynamic;
using System.Data.Entity;

namespace RC_API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class GroupsController : ApiController
    {
        //*********GROUPS START************//

        //Read all groups
        [System.Web.Http.Route("api/Groups/getAllGroups")]
        [System.Web.Mvc.HttpGet]
        public List<dynamic> getAllGroups()
        {
            //Database connection
            ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();
            db.Configuration.ProxyCreationEnabled = false;

            // !!--User Management--!!
            //string sessionId = Request.Headers.Authorization.ToString();
            //var user = db.Where(o => o.SessionID == sessionId).FirstOrDefault();
            //if(user != null)

            try
            {
                return getAllGroupsReturnList(db.Organisational_Group.ToList());
            }
            catch (Exception e)
            {
                dynamic toReturn = new ExpandoObject();
                toReturn.Error = e;
                return toReturn;
            }
        }

        public List<dynamic> getAllGroupsReturnList(List<Organisational_Group> allGroups)
        {
            //Database connection
            ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();
            db.Configuration.ProxyCreationEnabled = false;

            List<dynamic> dynamicGroups = new List<dynamic>();

            foreach (Organisational_Group thisGroup in allGroups)
            {
                dynamic dynamicGroup = new ExpandoObject();

                dynamicGroup.Description = thisGroup.Description;
                dynamicGroup.Size = thisGroup.Size;
                dynamicGroup.Address = thisGroup.Address;

                dynamicGroups.Add(dynamicGroup);
            }
            return dynamicGroups;
        }

        //[System.Web.Http.Route("api/Groups/getOrgGroups")]
        //[System.Web.Mvc.HttpPost]

        //public List<dynamic> getOrgGroups()
        //{
        //    //Database connection
        //    ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();
        //    db.Configuration.ProxyCreationEnabled = false;

        //    List<Organisational_Group> orgGroups = db.Organisational_Group.Include(zz => zz.Group_Type).ToList();

        //    try
        //    {
        //        return getOrgGroups(orgGroups);
        //    }
        //    catch (Exception e)
        //    {
        //        dynamic toRetrun = new ExpandoObject();
        //        toRetrun.Error = e;
        //        return toRetrun;
        //    }
        //}

        //public List<dynamic> getOrgGroups(List<Organisational_Group> group)
        //{
        //    List<dynamic> dynamicGroups = new List<dynamic>();
        //    foreach (Organisational_Group organisationalgroup in group)
        //    {
        //        dynamic dynamicGroup = new ExpandoObject();
        //        dynamicGroup.OrgGroupID = organisationalgroup.OrgGroupID;
        //        dynamicGroup.GroupTypeID = getAllOrgGroups(organisationalgroup);
        //        dynamicGroup.Description = organisationalgroup.Description;
        //        dynamicGroup.Size = organisationalgroup.Size;
        //        dynamicGroup.Address = organisationalgroup.Address;

        //        dynamicGroups.Add(dynamicGroup);
        //    }
        //    return dynamicGroups;
        //}

        //public List<dynamic> getAllOrgGroups(Organisational_Group organisationalgroup)
        //{
        //    List<dynamic> dynamicGroups = new List<dynamic>();
        //    foreach (Group_Type groupType in organisationalgroup.Group_Type)
        //    {
        //        dynamic dynamicGroupType = new ExpandoObject();
        //        dynamicGroupType.GroupTypeDescription = groupType.GroupTypeDescription;
        //        dynamicGroupType.GroupTypeName = groupType.GroupTypeName;

        //        dynamicGroups.Add(dynamicGroupType);
        //    }
        //    return dynamicGroups;
        //}

        [System.Web.Http.Route("api/Groups/addOrgGroups")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> addOrgGroup([FromBody] Organisational_Group orgGroup)
        {
            ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();
            db.Configuration.ProxyCreationEnabled = false;

            db.Organisational_Group.Add(orgGroup);

            try
            {
                db.SaveChanges();
                return getAllGroups();
            }
            catch (Exception e)
            {
                dynamic toReturn = new ExpandoObject();
                toReturn.Error = e;
                return toReturn;
            }
        }

        //Update organisational group
        [System.Web.Http.Route("api/Groups/updateOrgGroup")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> updateOrgGroup([FromBody] Organisational_Group currentGroup)
        {
            ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();
            db.Configuration.ProxyCreationEnabled = false;

            Organisational_Group thisOrgGroup = db.Organisational_Group.Where(a => a.OrgGroupID == currentGroup.OrgGroupID).FirstOrDefault();

            thisOrgGroup.OrgGroupID = currentGroup.OrgGroupID;
            thisOrgGroup.GroupTypeID = currentGroup.GroupTypeID;
            thisOrgGroup.Description = currentGroup.Description;
            thisOrgGroup.Size = currentGroup.Size;
            thisOrgGroup.Address = currentGroup.Address;
            thisOrgGroup.SuburbID = currentGroup.SuburbID;

            try
            {
                db.SaveChanges();
                return getAllGroups();
            }
            catch (Exception e)
            {
                dynamic toReturn = new ExpandoObject();
                toReturn.Error = e;
                return toReturn;
            }
        }

        //3.7 Add group type-Marno

        [System.Web.Http.Route("api/Groups/AddGroupType")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> AddCustomer([FromBody] Group_Type AddGroupType)
        {
            ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();
            if (AddGroupType != null)
            {

                db.Configuration.ProxyCreationEnabled = false;
                db.Group_Type.Add(AddGroupType);
                db.SaveChanges();
                return GetGroupTypes();
            }
            else
            {
                return null;
            }
        }


        //3.8 Search group type -Marno (This is also a view)
        [System.Web.Http.Route("api/Groups/GetGroupTypes")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> GetGroupTypes()
        {
            ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();

            db.Configuration.ProxyCreationEnabled = false;
            return GetGroupTypeList(db.Group_Type.ToList());
        }

        private List<dynamic> GetGroupTypeList(List<Group_Type> forClient)
        {
            List<dynamic> dynamicGroupTypes = new List<dynamic>();
            foreach (Group_Type grouptype in forClient)
            {
                dynamic dynamicGroupType = new ExpandoObject();
                //assign
                dynamicGroupType.GroupTypeID = grouptype.GroupTypeID;
                dynamicGroupType.GroupTypeName = grouptype.GroupTypeName;

                //add to origional
                dynamicGroupTypes.Add(dynamicGroupType);
            }
            return dynamicGroupTypes;
        }


        //3.9 Update group type - Marno
        [System.Web.Http.Route("api/Groups/UpdateGroupType")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> UpdateFood([FromBody] Group_Type newGroupType)
        {
            ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();

            Group_Type updater = db.Group_Type.Where(x => x.GroupTypeID == newGroupType.GroupTypeID).FirstOrDefault();
            updater.GroupTypeName = newGroupType.GroupTypeName;
            updater.GroupTypeDescription = newGroupType.GroupTypeDescription;

            db.Group_Type.Attach(updater);
            db.Entry(updater).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return GetGroupTypes();
        }

        //3.10 Delete group Type - Marno
        [System.Web.Http.Route("api/Groups/DeleteGroupType")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> deleteGroupType([FromBody] Group_Type groups)
        {
            ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();

            Group_Type group = db.Group_Type.Where(x => x.GroupTypeID == groups.GroupTypeID).FirstOrDefault();
            db.Group_Type.Remove(group);
            db.SaveChanges();
            return GetGroupTypes();
        }

    }
}