﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using RC_API.Models;
using System.Web.Http.Cors;
using System.Dynamic;

namespace RC_API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MessagesController : ApiController
    {
        //Send Invitation
        /*Create the invitation itsself*/
        [System.Web.Http.Route("api/Messages/createInvitation")]
        [System.Web.Mvc.HttpPost]
        public void createInvitation([FromBody] Invitation invitation)
        {
            ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();
            db.Configuration.ProxyCreationEnabled = false;
              
            // add to invitation table
            db.Invitations.Add(invitation);

            
            try
            {
                db.SaveChanges();
                // dont know if I need to return anything
            }
            catch (Exception e)
            {
                dynamic toReturn = new ExpandoObject();
                toReturn.Error = e;
                
            }



        }

        /*Get te selected receivers (Adds an entry in the Person/Invitation log) 
         
        -- I would receive an object from angular/ionic that already has the attributes set to match the user input. 
        */
        [System.Web.Http.Route("api/Messages/SendInvitation")]
        [System.Web.Mvc.HttpPost]
        public void SendInvitation([FromBody] Person_Invitation thisPerson_Invitation)
        {
            ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();
            db.Configuration.ProxyCreationEnabled = false;

            // add to Person/Invitation table
            db.Person_Invitation.Add(thisPerson_Invitation);

            try
            {
                db.SaveChanges();
                // dont know if I need to return anything
            }
            catch (Exception e)
            {
                dynamic toReturn = new ExpandoObject();
                toReturn.Error = e;

            }
            
        }


    }
}