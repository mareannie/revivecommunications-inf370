﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using RC_API.Models;
using System.Web.Http.Cors;
using System.Dynamic;


namespace RC_API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]

    public class ReportingController : ApiController
    {
        //2.13 report on Homecell attendance -Marno
        [System.Web.Http.Route("api/Reporting/ReportHCAttendance")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> ReportHCAttendance([FromBody] Homecell_Attendance_Feedback AddHCAttendance)
        {
            ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();
            if (AddHCAttendance != null)
            {

                db.Configuration.ProxyCreationEnabled = false;
                db.Homecell_Attendance_Feedback.Add(AddHCAttendance);
                db.SaveChanges();

                return null;
            }
            else
            {
                return null;
            }
        }

        //2.14 Report on church attendance- -Marno
        [System.Web.Http.Route("api/Reporting/ReportChurchAttendance")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> ReportChurchAttendance([FromBody] Church_Attendance_Feedback AddChurchAttendance)
        {
            ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();
            if (AddChurchAttendance != null)
            {

                db.Configuration.ProxyCreationEnabled = false;
                db.Church_Attendance_Feedback.Add(AddChurchAttendance);
                db.SaveChanges();

                return null;
            }
            else
            {
                return null;
            }
        }
    }
}
