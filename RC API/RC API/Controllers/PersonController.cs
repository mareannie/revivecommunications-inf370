﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RC_API.Models;

namespace RC_API.Controllers
{
   // [EnableCors(origins: "*", headers: "*", methods: "*")]

    public class PersonController : ApiController
    {
        ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();

        //Search Person
        //ALL USERS
        [HttpGet]
        [Route("AllPersons")]
        public IQueryable<Person> GetPerson()
        {
            try
            {
                return db.People;
            }
            catch (Exception)
            {
                throw;
            }
        }

        //USERS BY ID

        [HttpGet]
                [Route("GetPersonById/{PersonID}")]
        public IHttpActionResult GetPersonById(string PersonID)
        {
            Person objEmp = new Person();
            int ID = Convert.ToInt32(PersonID);
            try
            {
                objEmp = db.People.Find(ID);
                if (objEmp == null)
                {
                    return NotFound();
                }

            }
            catch (Exception)
            {
                throw;
            }

            return Ok(objEmp);
        }
    }
}
