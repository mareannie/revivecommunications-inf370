﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using System.Web.Http.Cors;
using RC_API.Models;
using System.Dynamic;
using System.Data.Entity;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Runtime.CompilerServices;

namespace RC_API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MembersController : ApiController
    {
        //*********Members START************//

        //Retrieve all Members from person table to view the Members and to approve the Members
        [System.Web.Http.Route("api/Members/getAllMembers")]
        [System.Web.Mvc.HttpGet]
        public List<dynamic> getAllMembers()
        {
            //Database connection
            ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();
            db.Configuration.ProxyCreationEnabled = false;

            // !!--User Management--!!
            //string sessionId = Request.Headers.Authorization.ToString();
            //var user = db.Where(o => o.SessionID == sessionId).FirstOrDefault();
            //if(user != null)

            try
            {
                return getAllMembersReturnList(db.People.Where(a => a.Activation_Status_ID == 1).ToList());
            }
            catch (Exception e)
            {
                dynamic toReturn = new ExpandoObject();
                toReturn.Error = e;
                return toReturn;
            }
        }

        public List<dynamic> getAllMembersReturnList(List<Person> allPeople)
        {
            //Database conncection
            ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();
            db.Configuration.ProxyCreationEnabled = false;

            List<dynamic> dynamicPeople = new List<dynamic>();

            foreach (Person person in allPeople)
            {
                dynamic dynamicPerson = new ExpandoObject();
                dynamicPerson.PersonID = person.PersonID;
                dynamicPerson.Name = person.Name;
                dynamicPerson.Surname = person.Surname;
                dynamicPerson.DateOfBirth = person.DateOfBirth;
                dynamicPerson.Number = person.Number;
                dynamicPerson.Email = person.Email;
                dynamicPerson.Address = person.Address;
                dynamicPerson.Activation_Status = person.Activation_Status_ID;

                dynamicPeople.Add(dynamicPerson);
            }
            return dynamicPeople;
        }

        [System.Web.Http.Route("api/Members/getAllUnapprovedMembers")]
        [System.Web.Mvc.HttpGet]
        public List<dynamic> getAllUnapprovedMembers()
        {
            //Database connection
            ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();
            db.Configuration.ProxyCreationEnabled = false;

            // !!--User Management--!!
            //string sessionId = Request.Headers.Authorization.ToString();
            //var user = db.Where(o => o.SessionID == sessionId).FirstOrDefault();
            //if(user != null)

            try
            {
                return getAllUnapprovedMembersReturnList(db.People.Where(a => a.Activation_Status_ID == 4).ToList());
            }
            catch (Exception e)
            {
                dynamic toReturn = new ExpandoObject();
                toReturn.Error = e;
                return toReturn;
            }
        }

        public List<dynamic> getAllUnapprovedMembersReturnList(List<Person> allPeople)
        {
            //Database conncection
            ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();
            db.Configuration.ProxyCreationEnabled = false;

            List<dynamic> dynamicPeople = new List<dynamic>();

            foreach (Person person in allPeople)
            {
                dynamic dynamicPerson = new ExpandoObject();
                dynamicPerson.PersonID = person.PersonID;
                dynamicPerson.Name = person.Name;
                dynamicPerson.Surname = person.Surname;
                dynamicPerson.DateOfBirth = person.DateOfBirth;
                dynamicPerson.Number = person.Number;
                dynamicPerson.Email = person.Email;
                dynamicPerson.Address = person.Address;
                dynamicPerson.Activation_Status = person.Activation_Status_ID;

                dynamicPeople.Add(dynamicPerson);
            }
            return dynamicPeople;
        }

        [System.Web.Http.Route("api/Members/updateMemberActivationStatus")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> updateMemberActivationStatus([FromBody] Person currentPerson)
        {
            //Database connection
            ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();
            db.Configuration.ProxyCreationEnabled = false;

            Person thisPerson = db.People.Where(z => z.PersonID == currentPerson.PersonID).FirstOrDefault();

            //Find the person current activation status id en updated the activation status id to approved.
            
            thisPerson.PersonID = currentPerson.PersonID;
            thisPerson.Activation_Status_ID = currentPerson.Activation_Status_ID;


            try
            {
                db.SaveChanges();
                return getAllMembers();
            }
            catch (Exception e)
            {
                dynamic toReturn = new ExpandoObject();
                toReturn.Error = e;
                return toReturn;
            }
        }


    }
}