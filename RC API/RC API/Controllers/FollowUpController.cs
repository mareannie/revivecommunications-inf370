﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RC_API.Models;
using System.Web.Http.Cors;
using System.Dynamic;
using System.Web.UI.WebControls;

namespace RC_API.Controllers
{
    public class FollowUpController : ApiController
    {

        //Calculate progress statistics
        ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3(); // create database connection

        //4.1 Follow-up on salvation - Charl
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        //Read All Salvation information
        [System.Web.Http.Route("api/FollowUp/Salvation")] //create route for api
        [System.Web.Mvc.HttpPost]

        public List<dynamic> Salvation()
        {
            db.Configuration.ProxyCreationEnabled = false; // configure proxy to eliminate overload of memory

            return SalvationInformation(db.Salvations.ToList()); // return called method
        }

        public List<dynamic> SalvationInformation(List<Salvation> SalvationTable)
        {
            List<dynamic> dynamicSalvationList = new List<dynamic>();

            //foreach method ro retrieve data from database and add it to list to return
            foreach (var Visitor in SalvationTable)
            {
                //create new dynamic object 
                dynamic dynamicSalvation = new ExpandoObject();
                dynamicSalvation.SalID = Visitor.SalID;
                dynamicSalvation.Date = Visitor.Date;
                dynamicSalvation.AlterWorker = Visitor.AlterWorker;
                dynamicSalvation.Name = Visitor.Name;
                dynamicSalvation.Surname = Visitor.Surname;
                dynamicSalvation.Age = Visitor.Age;
                dynamicSalvation.EmploymentStatus = Visitor.EmploymentStatus;
                dynamicSalvation.MaritialStatus = Visitor.MaritualStatus;
                dynamicSalvation.ResidentialAddress = Visitor.ResidentialAddress;
                dynamicSalvation.Suburb = Visitor.Suburb;
                dynamicSalvation.City = Visitor.City;
                dynamicSalvation.HomeTelNo = Visitor.HomeTelNo;
                dynamicSalvation.WorkTelNo = Visitor.WorkTelNo;
                dynamicSalvation.Cellphone = Visitor.CellPhone;
                dynamicSalvation.Email = Visitor.Email;
                dynamicSalvation.Invited = Visitor.Invited;
                dynamicSalvation.NameSurnameInvite = Visitor.NameSurnameInvite;
                dynamicSalvation.HomecellLeader = Visitor.HomecellLeader;
                dynamicSalvation.ZonePastor = Visitor.ZonePastor;
                dynamicSalvation.StudyAddress = Visitor.StudyAddress;
                dynamicSalvation.ParentGuardianCell = Visitor.ParentGuardianCell;
                dynamicSalvation.PrayerRequest = Visitor.PrayerRequest;
                dynamicSalvation.SchoolLevel = Visitor.SchoolLevel;
                dynamicSalvation.NameofSchool = Visitor.NameofSchool;
                dynamicSalvation.Grade = Visitor.Grade;
                dynamicSalvation.StudyYear = Visitor.StudyYear;
                dynamicSalvation.Institution = Visitor.Institution;
                dynamicSalvation.FollowedUp = Visitor.FollowedUp;
                dynamicSalvation.NoAnswer = Visitor.NoAnswer;
                dynamicSalvationList.Add(dynamicSalvation); // add to dynamic list
            }


            //create new dynamic object for calculations
            dynamic FollowUpProgress = new ExpandoObject();

            //Get list of data depending on remaining and completed
            List<Salvation> TotalFollowUps = db.Salvations.Where(x => x.FollowedUp == false && x.NoAnswer == false).ToList();
            List<Salvation> CompletedFollowUps = db.Salvations.Where(x => x.FollowedUp == true && x.NoAnswer == false).ToList();

            //Add list totals and perfom remaining calculations
            FollowUpProgress.Total = TotalFollowUps.Count;
            FollowUpProgress.Remaining = TotalFollowUps.Count - CompletedFollowUps.Count;
            FollowUpProgress.Completed = CompletedFollowUps.Count;

            //Add totals and numbers to list
            dynamicSalvationList.Add(FollowUpProgress);

            return dynamicSalvationList; //return list
        }

        //Follow-up Completed
        [System.Web.Http.Route("api/FollowUp/UpdateSalvationFollowUp")] //create route for api
        [System.Web.Mvc.HttpPost]
        public List<dynamic> UpdateSalvationFollowUp([FromBody] Salvation SalvationUpdate) //get JSON parameter
        {
            //validate that there is no null values
            if (SalvationUpdate != null)
            {
                db.Configuration.ProxyCreationEnabled = false; // configure proxy to eliminate overload of memory

                Salvation SalFollowUpUpdate = db.Salvations.Where(z => z.SalID == SalvationUpdate.SalID).FirstOrDefault(); //retrieve record to update data

                SalFollowUpUpdate.FollowedUp = true; //update boolean values
                SalFollowUpUpdate.NoAnswer = false;

                dynamic auditLog = new ExpandoObject();
                auditLog.PersonID = 1;
                auditLog.EventDescription = "Completed Follow-up on Salvation with ID:";
                auditLog.EventDateTime = DateTime.Now;
                db.Audit_Trail.Add(auditLog);


                db.SaveChanges(); //save changes 
                return Salvation();
            }
            else
            {
                return null;
            }
        }

        //Follow-up no Answer
        [System.Web.Http.Route("api/FollowUp/UpdateSalvationFollowUpNoAnswer")] //create route for api
        [System.Web.Mvc.HttpPost]
        public List<dynamic> UpdateSalvationFollowUpNoAnswer([FromBody] Salvation SalvationUpdate) //get JSON parameter
        {
            //validate that there is no null values
            if (SalvationUpdate != null)
            {

                db.Configuration.ProxyCreationEnabled = false; // configure proxy to eliminate overload of memory

                Salvation SalFollowUpUpdate = db.Salvations.Where(z => z.SalID == SalvationUpdate.SalID).FirstOrDefault(); //retrieve record to update data

                SalFollowUpUpdate.FollowedUp = false; //update boolean values
                SalFollowUpUpdate.NoAnswer = true;

                dynamic auditLog = new ExpandoObject();
                auditLog.PersonID = 1;
                auditLog.EventDescription = "No Answer for Follow-up on Salvation with ID:";
                auditLog.EventDateTime = DateTime.Now;
                db.Audit_Trail.Add(auditLog);

                db.SaveChanges(); //save changes 
                return Salvation();
            }
            else
            {
                return null;
            }
        }

        //4.6 Follow-up on Members wanting to serve - Charl

        //Read All Members wanting to serve information
        [System.Web.Http.Route("api/FollowUp/retrieveMembersWantingToServe")] //create route for api
        [System.Web.Mvc.HttpPost]

        public List<dynamic> retrieveMembersWantingToServe()
        {
            db.Configuration.ProxyCreationEnabled = false; // configure proxy to eliminate overload of memory

            return getMembersServeInformation(db.Members_Serve_Follow_Up.ToList()); // return called method
        }

        public List<dynamic> getMembersServeInformation(List<Members_Serve_Follow_Up> MemberServeList)
        {
            List<dynamic> dynamicMemberServe = new List<dynamic>();
            //foreach method ro retrieve data from database and add it to list to return
            foreach (var member in MemberServeList)
            {
                //create new dynamic object 
                dynamic dynamicMember = new ExpandoObject();
                dynamicMember.PersonName = member.Person.Name;
                dynamicMember.PersonSurname = member.Person.Surname;
                dynamicMember.PersonNumber = member.Person.Number;
                dynamicMember.PersonEmail = member.Person.Email;
                dynamicMember.ZonePastor = member.ZonePastor;
                dynamicMember.Homecell = member.Homecell;
                dynamicMember.SpiritualGiftTestSession = member.SpiritualGiftTestSession;
                dynamicMember.HighestSpiritualGifts = member.HighestSpiritualGifts;
                dynamicMember.Group1 = member.Group1;
                dynamicMember.Group2 = member.Group2;
                dynamicMember.Group3 = member.Group3;
                dynamicMember.Comment = member.Comment;
                dynamicMember.FollowedUp = member.FollowUpStatus;
                dynamicMember.NoAnswer = member.NoAnswer;
                dynamicMemberServe.Add(dynamicMember); // add to dynamic list
            }

            //Calculate progress statistics
            db.Configuration.ProxyCreationEnabled = false; // configure proxy to eliminate overload of memory

            //create new dynamic object for calculations
            dynamic FollowUpProgress = new ExpandoObject();

            //Get list of data depending on remaining and completed
            List<Members_Serve_Follow_Up> TotalFollowUps = db.Members_Serve_Follow_Up.Where(x => x.FollowUpStatus == false && x.NoAnswer == false).ToList();
            List<Members_Serve_Follow_Up> CompletedFollowUps = db.Members_Serve_Follow_Up.Where(x => x.FollowUpStatus == true && x.NoAnswer == false).ToList();

            //Add list totals and perfom remaining calculations
            FollowUpProgress.Total = TotalFollowUps.Count;
            FollowUpProgress.Remaining = TotalFollowUps.Count - CompletedFollowUps.Count;
            FollowUpProgress.Completed = CompletedFollowUps.Count;

            //Add calculations and numbers of prgress to the lists
            dynamicMemberServe.Add(FollowUpProgress);

            return dynamicMemberServe; //return the list
        }

        //Follow-up  Completed
        [System.Web.Http.Route("api/FollowUp/UpdateSalvationFollowUp")] //create route for api
        [System.Web.Mvc.HttpPost]
        public List<dynamic> UpdateSalvationFollowUp([FromBody] Members_Serve_Follow_Up MemberServeUpdate) //get JSON parameter
        {
            //validate that there is no null values
            if (MemberServeUpdate != null)
            {
                db.Configuration.ProxyCreationEnabled = false; // configure proxy to eliminate overload of memory

                Members_Serve_Follow_Up MemberServeFollowUpUpdate = db.Members_Serve_Follow_Up.Where(z => z.ServeRequestID == MemberServeUpdate.ServeRequestID).FirstOrDefault(); //retrieve record to update data

                MemberServeFollowUpUpdate.FollowUpStatus = false; //update boolean values
                MemberServeFollowUpUpdate.NoAnswer = true;

                dynamic auditLog = new ExpandoObject();
                auditLog.PersonID = 1;
                auditLog.EventDescription = "Completed Follow-up on Members wanting to serve with ID:";
                auditLog.EventDateTime = DateTime.Now;
                db.Audit_Trail.Add(auditLog);

                db.SaveChanges(); //save changes 
                return retrieveMembersWantingToServe();
            }
            else
            {
                return null;
            }
        }

        //Follow-up no Answer
        [System.Web.Http.Route("api/FollowUp/UpdateSalvationFollowUpNoAnswer")] //create route for api
        [System.Web.Mvc.HttpPost]
        public List<dynamic> UpdateSalvationFollowUpNoAnswer([FromBody] Members_Serve_Follow_Up MemberServeUpdate) //get JSON parameter
        {
            //validate that there is no null values
            if (MemberServeUpdate != null)
            {
                db.Configuration.ProxyCreationEnabled = false; // configure proxy to eliminate overload of memory

                Members_Serve_Follow_Up MemberServeFollowUpUpdate = db.Members_Serve_Follow_Up.Where(z => z.ServeRequestID == MemberServeUpdate.ServeRequestID).FirstOrDefault(); //retrieve record to update data

                MemberServeFollowUpUpdate.FollowUpStatus = false; //update boolean values
                MemberServeFollowUpUpdate.NoAnswer = true;

                dynamic auditLog = new ExpandoObject();
                auditLog.PersonID = 1;
                auditLog.EventDescription = "No Answer for Follow-up on Members wanting to serve with ID:";
                auditLog.EventDateTime = DateTime.Now;
                db.Audit_Trail.Add(auditLog);

                db.SaveChanges(); //save changes 
                return retrieveMembersWantingToServe();
            }
            else
            {
                return null;
            }
        }

        //4.5 Follow up on discipleship - Annie
        //Read All Members who do 
        [System.Web.Http.Route("api/FollowUp/getIncompleteDiscipleshipMembers")] //create route for api
        [System.Web.Mvc.HttpPost]
        public List<dynamic> getIncompleteDiscipleshipMembers()
        {
            db.Configuration.ProxyCreationEnabled = false; // configure proxy to eliminate overload of memory

            //create list of members that have already been followed up on in the current week 
            var dateCriteria = DateTime.Now.Date.AddDays(-7);
            List<Person_Discipleship> completeFollowUps = db.Person_Discipleship.OrderByDescending(x => x.FollowUpDate)
                .Where(o => o.FollowUpDate <= dateCriteria).ToList();

            int DiscipleshipCount = db.Discipleships.Count();

            //Create a list of people who have not completed all the discipleships
            List<Person> needDisc = db.People.Include("Person_Discipleship").Where(x => x.Person_Discipleship.Count < DiscipleshipCount).ToList();

            List<Person> needsFollowUp = new List<Person>();
            foreach (Person p in needDisc)
            {
                //If the person that does not have all 5 has not been called yet this week add to list.
                if (completeFollowUps.Any(x => x.PersonID == p.PersonID))
                {

                }
                else
                {
                    needsFollowUp.Add(p);
                }
            }

            return getMembersDiscipleshipInformation(needsFollowUp); // return called method
        }

        public List<dynamic> getMembersDiscipleshipInformation(List<Person> incompleteDisListc)
        {
            List<dynamic> toPassList = new List<dynamic>();

            dynamic toPass = new ExpandoObject();
           // List<dynamic> dynamicMemberDisc = new List<dynamic>();
           // //foreach method to retrieve data from database and add it to list to return
           
           // foreach (var member in incompleteDisListc)
           // {
           //     //create new dynamic object 
           //     dynamic dynamicMember = new ExpandoObject();
           //     dynamicMember.PersonName = member.Name;
           //     dynamicMember.PersonSurname = member.Surname;
           //     dynamicMember.PersonNumber = member.Number;
           //     dynamicMember.DiscipleshipCompletedList = member.Person_Discipleship.ToList();
           //     dynamicMember.AllDiscipleships = db.Discipleships.ToList();


           //     dynamicMemberDisc.Add(dynamicMember); // add to dynamic list
           // }
           //// toPass.MemberList = dynamicMemberDisc;

            //Calculate progress statistics
            db.Configuration.ProxyCreationEnabled = false; // configure proxy to eliminate overload of memory

            //create new dynamic object for calculations
            dynamic FollowUpProgress = new ExpandoObject();

            //Get list of data depending on remaining and completed
            // List<Person_Discipleship> TotalFollowUps = db.Person_Discipleship.Where(x => x.FollowedUp == false ).ToList();
            List<Person_Discipleship> CompletedFollowUps = db.Person_Discipleship.Where(x => x.FollowedUp == true && x.FollowUpDate == DateTime.Today).ToList();
            int TotalFollowUps = incompleteDisListc.Count;



            //Add list totals and perfom remaining calculations
            FollowUpProgress.Total = TotalFollowUps;
            FollowUpProgress.Remaining = TotalFollowUps - CompletedFollowUps.Count;
            FollowUpProgress.Completed = CompletedFollowUps.Count;
            FollowUpProgress.PersentageComplete = CompletedFollowUps.Count / TotalFollowUps * 100;

            //Add calculations and numbers of prgress to the lists
            toPass.FollowUpPorgress = FollowUpProgress;
            toPassList.Add(toPass);

            return toPassList; //return the list
        }


        //Follow-up  Completed
        [System.Web.Http.Route("api/FollowUp/UpdateDiscipleshipFollowUp")] //create route for api
        [System.Web.Mvc.HttpPost]
        public List<dynamic> UpdateDiscipleshipFollowUp([FromBody] Person_Discipleship DiscipleshipUpdate) //get JSON parameter
        {
            //validate that there is no null values
            if (DiscipleshipUpdate != null)
            {
                db.Configuration.ProxyCreationEnabled = false; // configure proxy to eliminate overload of memory

                Person thisPerson = db.People.Where(z => z.PersonID == DiscipleshipUpdate.PersonID).FirstOrDefault(); //retrieve record to update data

                thisPerson.Person_Discipleship.Add(DiscipleshipUpdate); //Add new completed dicipleship to members Person_Disc table


                //dynamic auditLog = new ExpandoObject();
                //auditLog.PersonID = 1;
                //auditLog.EventDescription = "Completed Follow-up on Discipleship with ID:";
                //auditLog.EventDateTime = DateTime.Now;
                //db.Audit_Trail.Add(auditLog);

                db.SaveChanges(); //save changes 
                return getIncompleteDiscipleshipMembers();
            }
            else
            {
                return null;
            }
        }

        [System.Web.Http.Route("api/FollowUp/getMemberDiscData")] //create route for api
        [System.Web.Mvc.HttpPost]
        public List<dynamic> getMemberDiscData()
        {
            //create list of members that have already been followed up on in the current week 
            var dateCriteria = DateTime.Now.Date.AddDays(-7);
            List<Person_Discipleship> completeFollowUps = db.Person_Discipleship.OrderByDescending(x => x.FollowUpDate)
                .Where(o => o.FollowUpDate <= dateCriteria).ToList();

           int DiscipleshipCount = db.Discipleships.Count();

            //Create a list of people who have not completed all the discipleships
            List<Person> needDisc = db.People.Include("Person_Discipleship").Where(x => x.Person_Discipleship.Count < DiscipleshipCount).ToList();
          

            List<Person> needsFollowUp = new List<Person>();
            foreach (Person p in needDisc)
            {
                //If the person that does not have all 5 has not been called yet this week add to list.
                if (completeFollowUps.Any(x => x.PersonID == p.PersonID))
                {

                }
                else
                {
                    needsFollowUp.Add(p);
                }
            }

            List<dynamic> dynamicMemberDisc = new List<dynamic>();
            //foreach method to retrieve data from database and add it to list to return



            foreach (var member in needsFollowUp)
            {

              //  List<Person_Discipleship> p = db.Person_Discipleship.Where(x => x.PersonID == member.PersonID).ToList();
                //create new dynamic object 
                dynamic dynamicMember = new ExpandoObject();
                dynamicMember.PersonName = member.Name;
                dynamicMember.PersonSurname = member.Surname;
                dynamicMember.PersonNumber = member.Number;
                dynamicMember.DiscipleshipCompletedList = db.Person_Discipleship.Where(x => x.PersonID == member.PersonID).ToList();
                dynamicMember.Something = db.Discipleships.Include("Person_Discipleship").ToList();



                dynamicMemberDisc.Add(dynamicMember); // add to dynamic list
            }
            return dynamicMemberDisc;
        }



    }

}

