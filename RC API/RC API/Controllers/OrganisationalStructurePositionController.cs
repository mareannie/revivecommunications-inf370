﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RC_API.Models;
using System.Web.Http.Cors;
using System.Dynamic;
using System.Web.UI.WebControls;

namespace RC_API.Controllers
{
    public class OrganisationalStructurePositionController : ApiController
    {
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        //3.15 Add Organisational structure position -Charl

        [System.Web.Http.Route("api/OrganisationalStructurePosition/CreateOrgStructPos")] //create route for api
        [System.Web.Mvc.HttpPost]

        public List<dynamic> CreateOrgStructPos([FromBody] Organisational_Structure_Position NewPosition) //get JSON parameter
        {
            //validate that there is no null values
            if (NewPosition != null)
            {
                ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();//establish database connection

                db.Configuration.ProxyCreationEnabled = false; //configure proxy to eliminate overload of data

                db.Organisational_Structure_Position.Add(NewPosition); //add new customer to customer table

                //dynamic auditLog = new ExpandoObject();
                //auditLog.PersonID = 1;
                //auditLog.EventDescription = "Created Organisational Structure Position with ID:";
                //auditLog.EventDateTime = DateTime.Now;
                //db.Audit_Trail.Add(auditLog);

                db.SaveChanges(); //Save nchanges and Add new position

                return getOrgStructPos(db.Organisational_Structure_Position.ToList()); // return called method
            }
            else
            {
                return null;
            }
        }

        //3.16 Assign Organisational structure position -Charl


        //3.17 View Organisational structure position -Charl
        [System.Web.Http.Route("api/OrganisationalStructurePosition/ViewOrgStructPos")] //create route for api
        [System.Web.Mvc.HttpPost]
        public List<dynamic> ViewOrgStructPos()
        {
            ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();//establish database connection

            db.Configuration.ProxyCreationEnabled = false; //configure proxy to eliminate overload of data

            return getOrgStructPos(db.Organisational_Structure_Position.ToList()); // return called method
        }

        public List<dynamic> getOrgStructPos(List<Organisational_Structure_Position> PositionList)
        {
            List<dynamic> dynamicOrgStructPositionList = new List<dynamic>();
            //foreach method ro retrieve data from database and add it to list to return
            foreach (var Position in PositionList)
            {
                //create new dynamic object 
                dynamic dynamicOrgStructPosition = new ExpandoObject();
                dynamicOrgStructPosition.PersonID = Position.PersonID;
                dynamicOrgStructPosition.OrgStructType = Position.Organisational_Structure_Type.Description;
                dynamicOrgStructPosition.OrgSTructReportTo = Position.OrgStructIDReportsTo;
                dynamicOrgStructPosition.Description = Position.Description;
                dynamicOrgStructPosition.orgGroup = Position.Organisational_Group.Description;
                dynamicOrgStructPosition.OrgStructLevel = Position.OrgStructLevel;
                dynamicOrgStructPosition.OrgGroupList = Position.Organisational_Group;
                dynamicOrgStructPositionList.Add(dynamicOrgStructPosition); // add to dynamic list
            }
            return dynamicOrgStructPositionList;
        }


        //3.18 Maintain Organisational structure position -Charl
    }
}
