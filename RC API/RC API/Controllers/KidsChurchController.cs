﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using System.Web.Http.Cors;
using RC_API.Models;
using System.Dynamic;
using System.Data.Entity;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Runtime.CompilerServices;

namespace RC_API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class KidsChurchController : ApiController
    {
        //*********KidsChurch START************//

        //Read all children
        [System.Web.Http.Route("api/KidsChurch/getAllChildren")]
        [System.Web.Mvc.HttpGet]
        public List<dynamic> getAllChildren()
        {
            //Database connection
            ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();
            db.Configuration.ProxyCreationEnabled = false;

            // !!--User Management--!!
            //string sessionId = Request.Headers.Authorization.ToString();
            //var user = db.Where(o => o.SessionID == sessionId).FirstOrDefault();
            //if(user != null)

            try
            {
                return getAllChildrenReturnList(db.Children.ToList());
            }
            catch (Exception e)
            {
                dynamic toReturn = new ExpandoObject();
                toReturn.Error = e;
                return toReturn;
            }
        }

        public List<dynamic> getAllChildrenReturnList(List<Child> allChildren)
        {
            //Database connection
            ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();
            db.Configuration.ProxyCreationEnabled = false;

            List<dynamic> dynamicChildren = new List<dynamic>();

            foreach (Child thisChild in allChildren)
            {
                dynamic dynamicChild = new ExpandoObject();

                dynamicChild.ChildID = thisChild.ChildID;
                dynamicChild.ChildName = thisChild.Name;
                dynamicChild.ChildSurname = thisChild.Surname;
                dynamicChild.ChildDateOfBirth = thisChild.DateOfBirth;

                dynamicChildren.Add(dynamicChild);
            }
            return dynamicChildren;
        }

        [System.Web.Http.Route("api/KidsChurch/getPersonChildren")]
        [System.Web.Mvc.HttpGet]

        public List<dynamic> getPersonChildren()
        {
            //Database connection
            ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();
            db.Configuration.ProxyCreationEnabled = false;

            List<Child> children = db.Children.Include(zz => zz.Person_Children).ToList();

            try
            {
                return getPersonChildren(children);
            }
            catch (Exception e)
            {
                dynamic toRetrun = new ExpandoObject();
                toRetrun.Error = e;
                return toRetrun;
            }
        }

        private List<dynamic> getPersonChildren(List<Child> children)
        {
            List<dynamic> dynamicChildren = new List<dynamic>();
            foreach (Child child in children)
            {
                dynamic dynamicChild = new ExpandoObject();
                dynamicChild.ChildID = child.ChildID;
                dynamicChild.ChildName = child.Name;
                dynamicChild.ChildSurname = child.Surname;
                dynamicChild.DateOfBirth = child.DateOfBirth;
                dynamicChild.ParentChild = getAllPersonChildren(child);

                dynamicChildren.Add(dynamicChild);
            }
            return dynamicChildren;
        }

        private List<dynamic> getAllPersonChildren(Child child)
        {
            List<dynamic> dynamicChildren = new List<dynamic>();
            foreach (Person_Children personChild in child.Person_Children)
            {
                dynamic dynamicPersonChild = new ExpandoObject();
                dynamicPersonChild.KidsChurchID = personChild.KidsChurchID;
                dynamicPersonChild.PersonID = personChild.PersonID;
                dynamicPersonChild.ChildID = personChild.ChildID;
                dynamicPersonChild.KCVolunteerConfirmation = personChild.KCVolunteerConfirmation;
                dynamicPersonChild.CheckIn = personChild.CheckIn;
                dynamicPersonChild.ChekcInDateTime = personChild.CheckInDateTime;
                dynamicPersonChild.SignOut = personChild.SignOut;
                dynamicPersonChild.SignOutDateTime = personChild.SignOutDateTime;

                dynamicChildren.Add(dynamicPersonChild);
            }
            return dynamicChildren;
        }

        

        //Add child
        [System.Web.Http.Route("api/KidsChurch/addChild")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> addChild([FromBody] Child child)
        {
            //Database connection
            ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();
            db.Configuration.ProxyCreationEnabled = false;

            
            db.Children.Add(child);
            
            

            try
            {
                db.SaveChanges();
                return getAllChildren();
            }
            catch (Exception e)
            {
                dynamic toReturn = new ExpandoObject();
                toReturn.Error = e;
                return toReturn;
            }
        }

        //Update child
        [System.Web.Http.Route("api/KidsChurch/updateChild")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> updateChild([FromBody] Child currentChild)
        {
            //Database connection
            ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();
            db.Configuration.ProxyCreationEnabled = false;

            Child thisChild = db.Children.Where(z => z.ChildID == currentChild.ChildID).FirstOrDefault();

            thisChild.ChildID = currentChild.ChildID;
            thisChild.Name = currentChild.Name;
            thisChild.Surname = currentChild.Surname;
            thisChild.DateOfBirth = currentChild.DateOfBirth;

            try
            {
                db.SaveChanges();
                return getAllChildren();
            }
            catch (Exception e)
            {
                dynamic toReturn = new ExpandoObject();
                toReturn.Error = e;
                return toReturn;
            }
        }

        [System.Web.Http.Route("api/KidsChurch/getPersonChild")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> getPersonChild()
        {
            //Database connection
            ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();
            db.Configuration.ProxyCreationEnabled = false;

            List<Person_Children> personChildren = db.Person_Children.Include(zz => zz.Person).Include(aa => aa.Child).Include(qq=> qq.KidsChurch).ToList();

            try
            {
                return getPersonChild(personChildren);
            }
            catch (Exception e)
            {
                dynamic toReturn = new ExpandoObject();
                toReturn.Error = e;
                return toReturn;
            }
        }

        private List<dynamic> getPersonChild(List<Person_Children> child)
        {
            List<dynamic> dynamicPersonChildren = new List<dynamic>();
            foreach (Person_Children personChild in child)
            {
                dynamic dynamicPersonChild = new ExpandoObject();
                dynamicPersonChild.KidsChurchID = personChild.KidsChurchID;
                dynamicPersonChild.PersonID = personChild.PersonID;
                dynamicPersonChild.ChildID = personChild.ChildID;
                dynamicPersonChild.KCVolunteerConfirmation = personChild.KCVolunteerConfirmation;
                dynamicPersonChild.CheckIn = personChild.CheckIn;
                dynamicPersonChild.CheckInDateTime = personChild.CheckInDateTime;
                dynamicPersonChild.SignOut = personChild.SignOut;
                dynamicPersonChild.SignOutDateTime = personChild.SignOutDateTime;

                dynamicPersonChildren.Add(dynamicPersonChild);
            }
            return dynamicPersonChildren;
        }

        [System.Web.Http.Route("api/KidsChurch/getKidsChurch")]
        [System.Web.Mvc.HttpGet]
        public List<dynamic> getKidsChurch()
        {
            //Database connection
            ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();
            db.Configuration.ProxyCreationEnabled = false;

            try
            {
                return KidsChurchReturnList(db.KidsChurches.ToList());
            }
            catch (Exception e)
            {
                dynamic toReturn = new ExpandoObject();
                toReturn.Error = e;
                return toReturn;
            }
        }

        public List<dynamic> KidsChurchReturnList(List<KidsChurch> kidsChurch)
        {
            //Database connection
            ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();
            db.Configuration.ProxyCreationEnabled = false;

            List<dynamic> dynamicKidsChurches = new List<dynamic>();

            foreach (KidsChurch thisKidsChurch in kidsChurch)
            {
                dynamic dynamicKidsChurch = new ExpandoObject();

                dynamicKidsChurch.KidsChurchID = thisKidsChurch.KidsChurchID;
                dynamicKidsChurch.KidschurchName = thisKidsChurch.KidsChurchName;
                dynamicKidsChurch.HeadOfChurch = thisKidsChurch.HeadOfChurch;
                dynamicKidsChurch.QRCode = thisKidsChurch.QRcode;

                dynamicKidsChurches.Add(dynamicKidsChurch);
            }
            return dynamicKidsChurches;
        }

        [System.Web.Http.Route("api/KidsChurch/getKidsChurchChild")]
        [System.Web.Mvc.HttpGet]
        public List<dynamic> getKidsChurchChild()
        {
            ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();
            db.Configuration.ProxyCreationEnabled = false;

            List<KidsChurch> kidsChurches = db.KidsChurches.Include(z => z.Person_Children).ToList();

            try
            {
                return getKidsChurchChild(kidsChurches);
            }
            catch (Exception e)
            {
                dynamic toReturn = new ExpandoObject();
                toReturn.Error = e;
                return toReturn;
            }
        }

        private List<dynamic> getKidsChurchChild(List<KidsChurch> kidsChurches)
        {
            List<dynamic> dynamicKidsChurches = new List<dynamic>();
            foreach (KidsChurch kidsChurch in kidsChurches)
            {
                dynamic dynamicKidsChurch = new ExpandoObject();
                dynamicKidsChurch.KidsChurchID = kidsChurch.KidsChurchID;
                dynamicKidsChurch.KidsChurchName = kidsChurch.KidsChurchName;
                dynamicKidsChurch.HeadOfChurch = kidsChurch.HeadOfChurch;
                dynamicKidsChurch.Person_Children = getKidsChurchChild(kidsChurch);

                dynamicKidsChurches.Add(dynamicKidsChurch);
            }
            return dynamicKidsChurches;
        }

        private List<dynamic> getKidsChurchChild(KidsChurch kidsChurch)
        {
            List<dynamic> dynamicKidsChurches = new List<dynamic>();
            foreach (Person_Children kidsChurchChild in kidsChurch.Person_Children)
            {
                dynamic dynamicKidsChurchChild = new ExpandoObject();
                dynamicKidsChurchChild.KidsChurchID = kidsChurchChild.KidsChurchID;
                dynamicKidsChurchChild.PersonID = kidsChurchChild.PersonID;
                dynamicKidsChurchChild.ChildID = kidsChurchChild.ChildID;
                dynamicKidsChurchChild.KCVolunteerConfirmation = kidsChurchChild.KCVolunteerConfirmation;
                dynamicKidsChurchChild.CheckIn = kidsChurchChild.CheckIn;
                dynamicKidsChurchChild.CheckInDateTime = kidsChurchChild.CheckInDateTime;
                dynamicKidsChurchChild.SignOut = kidsChurchChild.SignOut;
                dynamicKidsChurchChild.SignOutDateTime = kidsChurchChild.SignOutDateTime;

                dynamicKidsChurches.Add(dynamicKidsChurchChild);
            }
            return dynamicKidsChurches;
        }
        //Marno
        [System.Web.Http.Route("api/KidsChurch/RemoveChild")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> RemoveChild([FromBody] Person_Children kids)
        {
            ReviveCommunicationsDBEntities3 db = new ReviveCommunicationsDBEntities3();

            Person_Children kid = db.Person_Children.Where(x => x.ChildID == kids.ChildID).FirstOrDefault();
            db.Person_Children.Remove(kid);
            db.SaveChanges();
            return getKidsChurchChild();
        }
    }
}