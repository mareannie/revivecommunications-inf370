import { Component, OnInit } from '@angular/core';
import { NbToastrService } from '@nebular/theme';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-post-announcement',
  templateUrl: './post-announcement.component.html',
  styleUrls: ['./post-announcement.component.scss']
})


export class PostAnnouncementComponent implements OnInit {

  linearMode = true;

  toggleLinearMode() {
    this.linearMode = !this.linearMode;
  }

  form: FormGroup;
  Receivers: FormGroup;
  AnnouncementContent: FormGroup;
  ConfirmedAnnouncement: FormGroup;

  isChecked = [];
  selectedPerson =[];
  Persons = [
    {key: '1', text: "Person 1"},
    {key: '2', text: "Person 2"},
    {key: '3', text: "Person 3"}
    ];

  constructor(
    private toastrService: NbToastrService,
    private fb: FormBuilder
  ) { }


  ngOnInit() {
    this.Receivers = this.fb.group({
      Receivers: ['', Validators.required],
    });
    this.AnnouncementContent = this.fb.group({
      Message: ['', Validators.required],
    });
    this.ConfirmedAnnouncement = this.fb.group({
      Confirm: ['', Validators.required],
    });
  }

  onSubmit(Announcement){

  }
  checked = false;

  select(checked: boolean) {
    this.checked = checked;
  }

  SelectedReceivers() {
    this.Receivers.markAsDirty();
  }

  WriteAnnouncement() {
    this.AnnouncementContent.markAsDirty();
  }

  SendAnnouncement() {
    this.ConfirmedAnnouncement.markAsDirty();
  }

  showToast(position, status) {

    this.toastrService.show(
      status || 'Success',
      `Message was successfully sent.`,
      { position, status});
  }


}
