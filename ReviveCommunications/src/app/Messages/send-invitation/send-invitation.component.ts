import { Component, OnInit } from '@angular/core';
import { NbToastrService, NbDialogService } from '@nebular/theme';


@Component({
  selector: 'app-send-invitation',
  templateUrl: './send-invitation.component.html',
  styleUrls: ['./send-invitation.component.scss']
})
export class SendInvitationComponent implements OnInit {

  constructor(
    private toastrService: NbToastrService,
  ) { }

   //Dummy Data

   Groups: { name: string, type : string }[] = [{name:'Group1', type: 'Homecell'} , {name:'Group2', type: 'Volenteer'}, {name:'Group3', type: 'Kids Church'} ];
   Persons = [ "Person 1", "Person 2", "Person 3" ];


  ngOnInit(): void {
  }

  checked = false;

  select(checked: boolean) {
    this.checked = checked;
  }

  showToast(position, status) {

    this.toastrService.show(
      status || 'Success',
      `Message was successfully sent.`,
      { position, status});
  }

  onClick(){

  }



}
