export class IGroups
{
    OrgGroupID: number;
    GroupName: string;
    GroupDescription: string;
    GroupTypeID: number;
    Address: string;
    Size: number;
    SuburbID: number;
}