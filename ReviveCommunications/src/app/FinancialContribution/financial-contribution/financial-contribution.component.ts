import { Component, OnInit } from '@angular/core';
import { ChurchOption } from 'src/app/model/churchoption';
import { BankingInfo } from 'src/app/model/banking';
import { FinancialContributionService } from 'src/app/Services/financial-contribution.service';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-financial-contribution',
  templateUrl: './financial-contribution.component.html',
  styleUrls: ['./financial-contribution.component.scss']
})
export class FinancialContributionComponent implements OnInit {
  showTable = false;
  selectedOption: BankingInfo[];
  bankingDetails: false;
  showErrorMessage: boolean = false;

  financialContribution;
  ChurchBankID: BankingInfo[];

  options: ChurchOption[] /*= [
    { ChurchBankID: 1, ChurchName: 'CRC Pretoria' },
    { ChurchBankID: 2, ChurchName: 'CRC Bloemfontein' },
    { ChurchBankID: 3, ChurchName: 'CRC Johannesburg' },
  ]*/;
  bankingOptions: BankingInfo[] /*= [
    { ChurchBankID: 1, ChurchName: 'CRC Pretoria', AccountName: "CRC Pretoria", AccountNumber: "9164642662", Bank: "ABSA", BranchCode: "632005", Reference: "Name, Surname & Zone" },
    { ChurchBankID: 2, ChurchName: 'CRC Bloemfontein', AccountName: "CRC Bloemfontein", AccountNumber: "1102507490", Bank: "Nedbank", BranchCode: "110234", Reference: "Name, Surname & Zone" },
    { ChurchBankID: 3, ChurchName: 'CRC Johannesburg', AccountName: "CRC Johannesburg", AccountNumber: "9257341989", Bank: "ABSA", BranchCode: "632005", Reference: "Name, Surname & Zone" },
  ]*/;
  banking: Object;

  constructor(private financialContributionService: FinancialContributionService, private formBuilder: FormBuilder) {
    this.financialContribution = this.formBuilder.group(
      {
        ChurchBankID: ''
      }
    );
   }

  ngOnInit() {
    this.bankingDetails = false;
    this.financialContributionService.getChurchNames()
    .subscribe
    (
      bankdata =>
      {
        this.options = bankdata;
        console.log(bankdata);
      }
    );
  }

  showBanking(form) 
  {
    //this.ChurchBankID = form.ChurchBankID;
    console.log(form.ChurchBankID);

    this.financialContributionService.getChurchBanking(form.ChurchBankID).subscribe((res => {
      this.banking = res;
      console.log(res);
    }))
    //this.showTable = true;
  }
}

