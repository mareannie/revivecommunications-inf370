
import { FollowUpDiscService } from './../../Services/follow-up-disc.service';
import { Component, OnInit } from '@angular/core';
import { DiscipleshipService } from 'src/app/Services/discipleship.service';

@Component({
  selector: 'app-follow-up-discipleship',
  templateUrl: './follow-up-discipleship.component.html',
  styleUrls: ['./follow-up-discipleship.component.scss']
})
export class FollowUpDiscipleshipComponent implements OnInit {

  followUpList;
  persentageComplete;
  AllDiscipleships: any;
  completedDiscipleships: any[];
  MemberList

  Person_Discipleship =[];

  Discipleships;
  checkedIDs: any[];
  constructor(
    private service : FollowUpDiscService,
    private DiscService :DiscipleshipService,
  ) { }


  ngOnInit() {
    this.service.getFollowUpList().subscribe(x => {
      this.followUpList = x;
      console.log(x);
    });
    this.service.getMemberDiscData().subscribe(x => {
      this.MemberList = x;
      console.log(x);
    });

  }

  fetchCheckedIDs() {
    this.checkedIDs = []
    this.followUpList.forEach((value, index) => {
      if (value.isChecked) {
        this.checkedIDs.push(value.DiscipleshipID);
      }
    });
  }
  fetchSelectedItems() {

        this.Person_Discipleship = this.followUpList.filter((value, index) => {
          return value.isChecked
          });

  }

  changeSelection() {
    this.fetchSelectedItems()
    this.fetchCheckedIDs()



  }
  onSubmit(followUp){
    console.log(followUp);

  }

}
