import { Component, OnInit } from '@angular/core';
import { ISalvation } from '../../../model/salvation';
import { SalvationServiceService } from '../../../Salvation/salvation-service.service';

@Component({
  selector: 'app-follow-up-salvation',
  templateUrl: './follow-up-salvation.component.html',
  styleUrls: ['./follow-up-salvation.component.scss']
})
export class FollowUpSalvationComponent implements OnInit {


  //Create array of objects
Salvations: ISalvation[];

  constructor(private SalvationService: SalvationServiceService) { }

  ngOnInit(): void {
    this.SalvationService.Salvation().subscribe(data=> {this.Salvations = data;});


    
    console.log(this.Salvations);
  }

}
