import { Component, OnInit } from '@angular/core';
import { NbSidebarService, NbMenuItem, NbToastrService } from '@nebular/theme';
import { NbDialogService } from '@nebular/theme';
import { ViewMemberDialogComponent } from 'src/app/Person/view-member-dialog/view-member-dialog.component';

@Component({
  selector: 'app-nmo-follow-up',
  templateUrl: './nmo-follow-up.component.html',
  styleUrls: ['./nmo-follow-up.component.scss']
})
export class NmoFollowUpComponent implements OnInit {
  user = [
    { title: 'Profile' },
    { title: 'Logout' },
  ];

  constructor(private sidebarService: NbSidebarService, private toastrService: NbToastrService,
    private dialogService: NbDialogService) { }

open() {
this.dialogService.open(ViewMemberDialogComponent, {
  context: {

  },
});
}

  ngOnInit(): void {
  }

  toggle() {
    this.sidebarService.toggle(true);
    return false;
  }

  showToast(position, status) {

    this.toastrService.show(
      status || 'Success',
      `Member successfully followed up.`,
      { position, status});
  }

  showErrorToast(position, status) {

    this.toastrService.show(
      status || 'Danger',
      `Member not followed up`,
      { position, status});
  }

}

