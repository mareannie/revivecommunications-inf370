import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewInvitationComponent } from './Messages/view-invitation/view-invitation.component';
import { ViewAnnouncementComponent } from './Messages/view-announcement/view-announcement.component';
import { RemoveAnnouncementComponent } from './Messages/remove-announcement/remove-announcement.component';
import { SendInvitationComponent } from './Messages/send-invitation/send-invitation.component';
import { PostAnnouncementComponent } from './Messages/post-announcement/post-announcement.component';
import { ReportStructureGrowthComponent } from './Feedback/report-structure-growth/report-structure-growth.component';
import { NMOReportComponent } from './Feedback/nmoreport/nmoreport.component';
import { AddDiscipleshipComponent } from './Admin/Discipleship/add-discipleship/add-discipleship.component';
import { SearchDiscipleshipComponent } from './Admin/Discipleship/search-discipleship/search-discipleship.component';
import { UpdateDiscipleshipComponent } from './Admin/Discipleship/update-discipleship/update-discipleship.component';
import { ViewOrgIndivPosComponent } from './Admin/OrgIndivPos/view-org-indiv-pos/view-org-indiv-pos.component';
import { AddOrgIndivPosComponent } from './Admin/OrgIndivPos/add-org-indiv-pos/add-org-indiv-pos.component';
import { MaintainOrgIndivPosComponent } from './Admin/OrgIndivPos/maintain-org-indiv-pos/maintain-org-indiv-pos.component';
import { FollowUpDiscipleshipComponent } from './FollowUp/follow-up-discipleship/follow-up-discipleship.component';
import { RegisterChildComponent } from './KidsChurch/register-child/register-child.component';
import { OverviewStructureReportComponent } from './Reports/overview-structure-report/overview-structure-report.component';
import { FollowUpSalvationComponent } from './FollowUp/Salvation/follow-up-salvation/follow-up-salvation.component';
import { FollowUpMembersWantingToServeComponent } from './FollowUp/Members-wanting-to-serve/follow-up-members-wanting-to-serve/follow-up-members-wanting-to-serve.component';
import { SalvationViewComponent } from './FollowUp/Salvation/salvation-view/salvation-view.component';
import { FinancialContributionComponent } from './FinancialContribution/financial-contribution/financial-contribution.component';
import { GroupsComponent } from './Groups/groups/groups.component';
import { GroupTransferComponent } from './Groups/group-transfer/group-transfer.component';
import { AddGroupComponent } from './Groups/add-group/add-group.component';
import { MaintainGroupComponent } from './Groups/maintain-group/maintain-group.component';
import { MembersComponent } from './Person/members/members.component';
import { AssignLeaderComponent } from './Person/assign-leader/assign-leader.component';
import { ApproveMemberComponent } from './Person/approve-member/approve-member.component';
import { NmoFollowUpComponent } from './FollowUp/nmo-follow-up/nmo-follow-up.component';
import { LeaderFollowUpComponent } from './FollowUp/leader-follow-up/leader-follow-up.component';
import { ChildrenComponent } from './KidsChurch/children/children.component';
import { VolunteerChildrenComponent } from './KidsChurch/volunteer-children/volunteer-children.component';
import { SignOutChildComponent } from './KidsChurch/sign-out-child/sign-out-child.component';
import { VolunteerSignOutApproveComponent } from './KidsChurch/volunteer-sign-out-approve/volunteer-sign-out-approve.component';
import { UpdateChildComponent } from './KidsChurch/update-child/update-child.component';
import { StructureDiscipleshipComponent } from './Reports/structure-discipleship/structure-discipleship.component';
import { ZoneGrowthFeedbackComponent } from './Feedback/zone-growth-feedback/zone-growth-feedback.component';
import { ZoneHomecellAttendanceFeedbackComponent } from './Feedback/zone-homecell-attendance-feedback/zone-homecell-attendance-feedback.component';
import { ZoneChurchAttendanceFeedbackComponent } from './Feedback/zone-church-attendance-feedback/zone-church-attendance-feedback.component';
import { AddChildComponent } from './KidsChurch/add-child/add-child.component';
import { SalvationFormComponent } from './Salvation/salvation-form/salvation-form.component';
import { CreateOrganisationalStrcuturePositionComponent } from './Admin/OrgStructPos/Create-Organisational-Strcuture-Position/create-organisational-strcuture-position/create-organisational-strcuture-position.component';
import { CancelConfirmationDialogComponent } from './Admin/OrgStructPos/cancel-confirmation-dialog/cancel-confirmation-dialog.component';
import { ViewOrganisationalStrcuturePositionComponent } from './Admin/OrgStructPos/View-Organisational-Strcuture-Position/view-organisational-strcuture-position/view-organisational-strcuture-position.component';
import { LoginComponent } from './login/login/login.component';

const routes: Routes = [
  //Messages
  { path: 'SendInvitation', component: SendInvitationComponent },
  { path: 'PostAnnouncement', component: PostAnnouncementComponent },
  { path: 'ViewInvitation', component: ViewInvitationComponent },
  { path: 'ViewAnnouncement', component: ViewAnnouncementComponent },
  { path: 'RemoveAnnouncement', component: RemoveAnnouncementComponent },

  //Report & Feedback
  { path: 'ReportStructureGowth', component: ReportStructureGrowthComponent },
  { path: 'NMOReport', component: NMOReportComponent },

  //CRUDs
  { path: 'AddDiscipleship', component: AddDiscipleshipComponent },
  { path: 'SearchDiscipleship', component: SearchDiscipleshipComponent },
  { path: 'UpdateDiscipleship/:id', component: UpdateDiscipleshipComponent },
  { path: 'ViewOrgIndivPos', component: ViewOrgIndivPosComponent },
  { path: 'AddOrgIndivPos', component: AddOrgIndivPosComponent },
  { path: 'MaintainOrgIndivPos/:id', component: MaintainOrgIndivPosComponent },
  { path: 'AddChild', component: AddChildComponent },
  { path: 'UpdateChild/:id', component: UpdateChildComponent },
  { path: 'AddGroup', component: AddGroupComponent },
  { path: 'MaintainGroup', component: MaintainGroupComponent },
  { path: 'SalvationForm', component: SalvationFormComponent},
  { path: 'AddOrganisationalStructurePosition', component: CreateOrganisationalStrcuturePositionComponent },
  { path: 'CancelConfirmation', component: CancelConfirmationDialogComponent },
  { path: 'ViewOrganisationalStructurePosition', component: ViewOrganisationalStrcuturePositionComponent },

 


  //Follow Ups
  { path: 'FollowUpDiscipleship', component: FollowUpDiscipleshipComponent },
  { path: 'FollowUpSalvation' , component: FollowUpSalvationComponent },
  {path: 'SalvationView', component: SalvationViewComponent},
  { path: 'FollowUpMembersWantingToServe' , component: FollowUpMembersWantingToServeComponent },
  { path: 'NMOFollowUp', component: NmoFollowUpComponent },
  { path: 'LeaderFollowUp', component: LeaderFollowUpComponent },


  //Kids Church
  { path: 'RegisterChild', component: RegisterChildComponent },
  { path: 'SignOutChild', component: SignOutChildComponent },
  { path: 'VolunteerSignOutApprove', component: VolunteerSignOutApproveComponent },
  { path: 'VolunteerChildren', component: VolunteerChildrenComponent} ,
  { path: 'KidsChurch', component: ChildrenComponent },


  //Reports
  { path: 'OverviewStructureReport', component: OverviewStructureReportComponent },
  { path: 'StructureDiscipleshipReport', component: StructureDiscipleshipComponent },

  //Financial Contribution
  { path: 'FiancialContribution', component: FinancialContributionComponent },

  //Goal Feedback
  { path: 'ZoneGrowthFeedback', component: ZoneGrowthFeedbackComponent },
  { path: 'ZoneHomecellAttendanceFeedback', component: ZoneHomecellAttendanceFeedbackComponent },
  { path: 'ZoneChurchAttendanceFeedback', component: ZoneChurchAttendanceFeedbackComponent },

  //Groups
  { path: 'Groups', component: GroupsComponent },
  { path: 'GroupTransfer', component: GroupTransferComponent },

  //Members
  { path: 'ApproveMember', component: ApproveMemberComponent },
  { path: 'AssignLeader', component: AssignLeaderComponent },
  { path: 'Members', component: MembersComponent },
  
  //Login
  { path: 'Login', component: LoginComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
