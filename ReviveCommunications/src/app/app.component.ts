import { Component } from '@angular/core';
import { NbMenuItem, NbSidebarService, NbToastrService, NbDialogService, NbComponentStatus, NbIconConfig } from '@nebular/theme';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ReviveCommunications';

  constructor(
    private sidebarService: NbSidebarService,
    private toastrService: NbToastrService,
    private dialogService: NbDialogService,

  ) { }

  items: NbMenuItem[] = [

    {
      title: 'Profile',
      icon: 'person-outline',
      children: [
        {
          title: 'Update Profile',
          icon: 'edit-outline',
           link: '#'
        },
        {
          title: 'Financial Contribution',
          icon: 'credit-card-outline',
           link: '#'
        },
        {
          title: 'Logout',
          icon: 'unlock-outline',
           link: '#'
        },

      ],
    },
    {
      title: 'Manage Members',
      icon: 'people-outline',
      children: [
        {
          title: 'View Members',
          icon: 'eye-outline',
           link: '#'
        },
        {
          title: 'Activate Members',
          icon: 'person-done-outline',
           link: '#'
        },
        {
          title: 'Deactivate Members',
          icon: 'person-remove-outline',
           link: '#'
        },
        {
          title: 'Transfer Members',
          icon: 'swap-outline',
           link: '#'
        },

      ],

    },
    {
      title: 'Message Members',
      icon: 'email-outline',
      children: [
        {
          title: 'Send Invitation',
          icon: 'arrow-right-outline',
           link: 'SendInvitation'

        },
        {
          title: 'Send Announcement',
          icon: 'arrow-right-outline',
           link: 'PostAnnouncement'
        },
        {
          title: 'Remove Announcement',
          icon: 'arrow-right-outline',
           link: 'RemoveAnnouncement'
        },
      ],

    },
     {
      title: 'Groups',
      icon: 'plus-circle-outline',
       link: '#'
    },
     {
      title: 'Homecell notes',
      icon: 'file-add-outline',
       link: '#'
    },
     {
      title: 'Kids Church',
      icon: 'home-outline',
       link: '#'

    },

    {
      title: 'Follow-up',
      icon: 'phone-outline',
      children: [
        {
          title: 'Salvation',
          icon: 'arrow-right-outline',
           link: 'FollowUpSalvation'

        },
        {
          title: 'Requests to Serve',
          icon: 'arrow-right-outline',
           link: 'FollowUpMembersWantingToServe'
        },
        {
          title: 'NMO',
          icon: 'arrow-right-outline',
           link: '#'
        },
        {
          title: 'Overseers',
          icon: 'arrow-right-outline',
           link: '#'
        },
        {
          title: 'Leaders',
          icon: 'arrow-right-outline',
           link: '#'
        },
        {
          title: 'Members',
          icon: 'arrow-right-outline',
           link: '#'
        },
        {
          title: 'Discipleship',
          icon: 'arrow-right-outline',
           link: 'FollowUpDiscipleship'
        },
      ],

    },
    {
      title: 'Set Goals',
      icon: 'award-outline',
       link: '#'

    },
    {
      title: 'Feedback',
      icon: 'bar-chart',
      children: [
        {
          title: 'Homecell Attendance',
          icon: 'arrow-right-outline',
           link: '#'

        },
        {
          title: 'Church Attendance',
          icon: 'arrow-right-outline',
           link: '#'
        },
        {
          title: 'Discipleship Attendance',
          icon: 'arrow-right-outline',
           link: '#'
        },
        {
          title: 'Structure Growth',
          icon: 'arrow-right-outline',
           link: 'ReportStructureGowth'
        },
        {
          title: 'New Member Orientation',
          icon: 'arrow-right-outline',
           link: 'NMOReport'
        },
        {
          title: 'Zone Growth',
          icon: 'arrow-right-outline',
           link: '#'
        },
        {
          title: 'Zone HC Attendance',
          icon: 'arrow-right-outline',
           link: '#'
        },
        {
          title: 'Zone Church Attendance',
          icon: 'arrow-right-outline',
           link: '#'
        },

      ],

    },
    {
      title: 'Reports',
      icon: 'trending-up-outline',
      children: [
        {
          title: 'Zone Growth',
          icon: 'arrow-right-outline',
           link: '#'

        },
        {
          title: 'Overview Of Structure',
          icon: 'arrow-right-outline',
           link: 'OverviewStructureReport'
        },
        {
          title: 'Discipleship Progress',
          icon: 'arrow-right-outline',
           link: '#'
        },
      ],

    },
    {
      title: 'Admin',
      icon: 'settings-2-outline',
      children: [
        {
          title: 'User Roles',
          icon: 'arrow-right-outline',
           link: '#'

        },
        {
          title: 'Group Types',
          icon: 'arrow-right-outline',
           link: '#'

        },
        {
          title: 'Discipleships',
          icon: 'arrow-right-outline',
           link: 'SearchDiscipleship'
        },
        {
          title: 'Structure Positions',
          icon: 'arrow-right-outline',
           link: 'ViewOrganisationalStructurePosition'
        },
        {
          title: 'Individual Positions',
          icon: 'arrow-right-outline',
           link: 'ViewOrgIndivPos'
        },
        {
          title: 'Individual Position Goals',
          icon: 'arrow-right-outline',
           link: '#'
        },
      ],

    },
  ];


  toggle() {
    this.sidebarService.toggle(true);
    return false;
  }

}
