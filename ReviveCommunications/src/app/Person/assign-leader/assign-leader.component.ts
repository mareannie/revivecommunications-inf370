import { Component, OnInit } from '@angular/core';
import { NbSidebarService, NbMenuItem, NbToastrService } from '@nebular/theme';
import { NbDialogService } from '@nebular/theme';
import { ViewMemberDialogComponent } from 'src/app/Person/view-member-dialog/view-member-dialog.component';
import { IMembers } from 'src/app/model/members';
import { MembersService } from 'src/app/Services/members.service';
import { OrgIndivPos } from 'src/app/model/OrgIndivPos';
import { OrgIndivPosService } from 'src/app/Services/org-indiv-pos.service';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-assign-leader',
  templateUrl: './assign-leader.component.html',
  styleUrls: ['./assign-leader.component.scss']
})
export class AssignLeaderComponent implements OnInit {

  checked = false;
  members: IMembers[];
  positions: OrgIndivPos[];
  assignLeader
  
  constructor(private sidebarService: NbSidebarService, private toastrService: NbToastrService,
              private dialogService: NbDialogService, private memberService: MembersService, 
              private orgIndivPosService: OrgIndivPosService, private formBuilder: FormBuilder) 
              {
                this.assignLeader = this.formBuilder.group(
                  {
                    OrgIndivPosID: ''
                  }
                );
              }

    open() {
      this.dialogService.open(ViewMemberDialogComponent, {
        context: {
  
        },
      });
    }

    ngOnInit() 
    {
     this.memberService.getMembers()
     .subscribe
     (
       data => {
         this.members = data;
         console.log(data);
       }
     );

     this.orgIndivPosService.getAllOrgIndivPos()
     .subscribe
     (
       data => {
         this.positions = data;
         console.log(data);
       }
     );
    }

  toggle() {
    this.sidebarService.toggle(true);
    return false;
  }

  showToast(position, status) {

    if (this.checked = true)
    {
      this.toastrService.show(
        status || 'Success',
        `Leader successfully assigned.`,
        { position, status});
    }
    else
    {
      this.toastrService.show(
        status || 'Danger',
        `No Leader was assigned`,
        { position, status});
    }

    
  }

  showErrorToast(position, status) {

    this.toastrService.show(
      status || 'Danger',
      `No Leader was assigned`,
      { position, status});
  }

}

