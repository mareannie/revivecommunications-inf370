import { Component, OnInit } from '@angular/core';
import { NbSidebarService, NbMenuItem, NbToastrService } from '@nebular/theme';
import { LeaderOption } from 'src/app/model/leaderoptions';
import { NbDialogService } from '@nebular/theme';
import { ViewMemberDialogComponent } from 'src/app/Person/view-member-dialog/view-member-dialog.component';
import { IMembers } from 'src/app/model/members';
import { MembersService } from 'src/app/Services/members.service';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-approve-member',
  templateUrl: './approve-member.component.html',
  styleUrls: ['./approve-member.component.scss']
})
export class ApproveMemberComponent implements OnInit {

  checked = false;
  members: IMembers[];
  approveMember;


  constructor(private toastrService: NbToastrService, 
              private memberService: MembersService, 
              private dialogService: NbDialogService,
              private formBuilder: FormBuilder ) 
              {
                this.approveMember = this.formBuilder.group(
                {
                  Approved: ""
                }
                );
              }

  ngOnInit() 
  {
   this.memberService.getUnapprovedMembers()
   .subscribe
   (
     data => {
       this.members = data;
       console.log(data);
     }
   ); 
  }

  open() {
    this.dialogService.open(ViewMemberDialogComponent, {
      context: {

      },
    });
  }  

  showToast(position, status) {

    if (this.checked = true)
    {
      this.toastrService.show(
        status || 'Success',
        `Member successfully approved.`,
        { position, status});
    }
    else
    {
      this.toastrService.show(
        status || 'Danger',
        `Member not found`,
        { position, status});
    }

    
  }

  showErrorToast(position, status) {

    this.toastrService.show(
      status || 'Danger',
      `Member not found`,
      { position, status});
  }

  onSubmit(form)
  {
    console.log(form);

    this.memberService.updateActivationStatus(form);
  }

}

