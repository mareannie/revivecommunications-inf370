import { Component, OnInit } from '@angular/core';
import { NbToastrService } from '@nebular/theme';
import { LeaderOption } from 'src/app/model/leaderoptions';
import { IMembers } from 'src/app/model/members';
import { MembersService } from 'src/app/Services/members.service';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss']
})
export class MembersComponent implements OnInit {
  
  checked = false;
  members: IMembers[];


  constructor(private toastrService: NbToastrService, private memberService: MembersService) { }

  ngOnInit() 
  {
   this.memberService.getMembers()
   .subscribe
   (
     data => {
       this.members = data;
       console.log(data);
     }
   ); 
  }

  

  showToast(position, status) {

    if (this.checked = true)
    {
      this.toastrService.show(
        status || 'Success',
        `Member successfully approved.`,
        { position, status});
    }
    else
    {
      this.toastrService.show(
        status || 'Danger',
        `Member not found`,
        { position, status});
    }

    
  }

  showErrorToast(position, status) {

    this.toastrService.show(
      status || 'Danger',
      `Member not found`,
      { position, status});
  }

}
