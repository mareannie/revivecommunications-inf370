import { Component, OnInit } from '@angular/core';
import { NbSidebarService, NbMenuItem, NbToastrService } from '@nebular/theme';
import { NbDialogService } from '@nebular/theme';
import { ViewGroupDialogComponent } from 'src/app/Groups/view-group-dialog/view-group-dialog.component';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss']
})
export class GroupsComponent implements OnInit {
  data = [
    {
      id: 1,
      name: "Homecell",
    },
    {
      id: 2,
      name: "Social Welfare",
    },
    {
      id: 3,
      name: "Youth  Student Ministry",
    },
    // ... list of items

  ];

  user = [
    { title: 'Profile' },
    { title: 'Logout' },
  ];

  constructor(private sidebarService: NbSidebarService, private toastrService: NbToastrService,
    private dialogService: NbDialogService) { }
  
    open() {
      this.dialogService.open(ViewGroupDialogComponent, {
        context: {
  
        },
      });
    }

  settings = {
    selectMode: 'multi',
    hideSubHeader: true,
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
    columns: {
      name: {
        title: 'Group Name',  
      },
    },
  };


  ngOnInit(): void {
  }

  toggle() {
    this.sidebarService.toggle(true);
    return false;
  }

  showToast(position, status) {

    this.toastrService.show(
      status || 'Success',
      `Member successfully approved.`,
      { position, status});
  }

  showErrorToast(position, status) {

    this.toastrService.show(
      status || 'Danger',
      `Member not approved.`,
      { position, status});
  }

}
