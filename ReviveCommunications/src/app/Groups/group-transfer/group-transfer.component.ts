import { Component, OnInit } from '@angular/core';
import { NbSidebarService, NbMenuItem, NbToastrService } from '@nebular/theme';

@Component({
  selector: 'app-group-transfer',
  templateUrl: './group-transfer.component.html',
  styleUrls: ['./group-transfer.component.scss']
})
export class GroupTransferComponent implements OnInit {
  data = [
    {
      id: 1,
      name: "Homecell",
    },
    {
      id: 2,
      name: "Social Welfare",
    },
    {
      id: 3,
      name: "Youth  Student Ministry",
    },
    // ... list of items

  ];

  user = [
    { title: 'Profile' },
    { title: 'Logout' },
  ];

  checked = false;

  constructor(private sidebarService: NbSidebarService, private toastrService: NbToastrService,) { }

  settings = {
    hideSubHeader: true,
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
    columns: {
      name: {
        title: 'Group Name'
      },
    }
  };
  
  ngOnInit(): void {
  }

  toggle() {
    this.sidebarService.toggle(true);
    return false;
  }

  showToast(position, status) {

    if (this.checked = true)
    {
      this.toastrService.show(
        status || 'Success',
        `Transfer request sent successfully`,
        { position, status});
    }
    else
    {
      this.toastrService.show(
        status || 'Danger',
        `Not all field were completed. Please complete all required fields`,
        { position, status});
    }

    
  }

  showErrorToast(position, status) {

    this.toastrService.show(
      status || 'Danger',
      `Not all field were completed. Please complete all required fields`,
      { position, status});
  }
    
}

