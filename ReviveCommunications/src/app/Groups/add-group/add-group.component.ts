import { Component, OnInit } from '@angular/core';
import { NbSidebarService, NbMenuItem, NbToastrService } from '@nebular/theme';
import { FormBuilder, FormGroup } from '@angular/forms';
import { GroupsService } from 'src/app/Services/groups.service';
import { IGroups } from 'src/app/model/groups';

export interface tempGroup
{
  OrgGroupID: number;
  GroupTypeID: number; 
  GroupName: string;
  Address: string;
  SuburbID: number;
  CityID: number;
  ProvinceID: number;
  CountryID: number;
  Size: number;
}

@Component({
  selector: 'app-add-group',
  templateUrl: './add-group.component.html',
  styleUrls: ['./add-group.component.scss']
})
export class AddGroupComponent implements OnInit {
  
  groups: IGroups[];
  addGroup;

  checked = false;

  constructor(private sidebarService: NbSidebarService, 
              private toastrService: NbToastrService,
              private formbuilder: FormBuilder,
              private groupService: GroupsService) 
              {
                this.addGroup = this.formbuilder.group(
                  {
                    GroupTypeID: '',
                    GroupName: '',
                    Address: '',
                    SuburbID: '',
                    CityID: '',
                    ProvinceID: '',
                    CountryID: '',
                    Size: ''
                  }
                );
              }

  ngOnInit()
  {
    this.groupService.getGroups().subscribe
    (
      data => 
      {
        this.groups = data;
        console.log(data)
      }
    )
  }

  toggle() {
    this.sidebarService.toggle(true);
    return false;
  }

  showToast(position, status) {

    if (this.checked = true)
    {
      this.toastrService.show(
        status || 'Success',
        `Group successfully added.`,
        { position, status});
    }
    else
    {
      this.toastrService.show(
        status || 'Danger',
        `Not all field were completed. Please complete all required fields`,
        { position, status});
    }

    
  }

  showErrorToast(position, status) {

    this.toastrService.show(
      status || 'Danger',
      `Not all field were completed. Please complete all required fields`,
      { position, status});
  }

  onSubmit(groupname: string, groupType: number, address: string, size: number, suburb: number, city: number, province: number, country: number, event: Event)
  {
    event.preventDefault();
    var groupData = { GroupName: groupname, GroupTypeID: groupType, Address: address, Size: size, SuburbID: suburb, CityID: city, ProvinceID: province, CountryID: country} as tempGroup;
    this.groupService.addGroup(groupData);
  }

}
