import { Component, OnInit } from '@angular/core';
import { NbSidebarService, NbMenuItem, NbToastrService } from '@nebular/theme';
import { FormBuilder, FormGroup } from '@angular/forms';
import { GroupsService } from 'src/app/Services/groups.service';
import { IGroups } from 'src/app/model/groups';

export interface tempGroup
{
  OrgGroupID: number;
  GroupTypeID: number; 
  GroupName: string;
  Address: string;
  SuburbID: number;
  CityID: number;
  ProvinceID: number;
  CountryID: number;
  Size: number;
}

@Component({
  selector: 'app-maintain-group',
  templateUrl: './maintain-group.component.html',
  styleUrls: ['./maintain-group.component.scss']
})
export class MaintainGroupComponent implements OnInit {

  groups: IGroups[];
  updateGroup;
  group: any;

  checked = false;

  constructor(private sidebarService: NbSidebarService, 
              private toastrService: NbToastrService,
              private formbuilder: FormBuilder,
              private groupService: GroupsService) 
              {
                this.updateGroup = this.formbuilder.group(
                  {
                    GroupTypeID: '',
                    GroupName: '',
                    Address: '',
                    SuburbID: '',
                    CityID: '',
                    ProvinceID: '',
                    CountryID: '',
                    Size: ''
                  }
                );
              }

  ngOnInit() 
  {
    this.group = this.groupService.getData();
    console.log(this.group);
    if (this.group != null)
    {
      this.updateGroup.controls.GroupTypeID.setValue(this.group.GroupTypeID);
      this.updateGroup.controls.GroupName.setValue(this.group.GroupName);
      this.updateGroup.controls.Groupname.setValue(this.group.Groupname);
      this.updateGroup.controls.Address.setValue(this.group.Address);
      this.updateGroup.controls.SuburbID.setValue(this.group.SuburbID);
      this.updateGroup.controls.CityID.setValue(this.group.CityID);
      this.updateGroup.controls.ProvinceID.setValue(this.group.ProvinceID);
      this.updateGroup.controls.CountryID.setValue(this.group.CountryID);
      this.updateGroup.controls.Size.setValue(this.group.Size);      
    }
  }


  toggle() {
    this.sidebarService.toggle(true);
    return false;
  }

  showToast(position, status) {

    if (this.checked = true)
    {
      this.toastrService.show(
        status || 'Success',
        `Group updated successfully`,
        { position, status});
    }
    else
    {
      this.toastrService.show(
        status || 'Danger',
        `Not all field were completed. Please complete all required fields`,
        { position, status});
    }

    
  }

  showErrorToast(position, status) {

    this.toastrService.show(
      status || 'Danger',
      `Not all field were completed. Please complete all required fields`,
      { position, status});
  }

  onSubmit(groupname: string, groupType: number, address: string, size: number, suburb: number, city: number, province: number, country: number, event: Event)
  {
    event.preventDefault();
    var groupData = { GroupName: groupname, GroupTypeID: groupType, Address: address, Size: size, SuburbID: suburb, CityID: city, ProvinceID: province, CountryID: country} as tempGroup;
    this.groupService.addGroup(groupData);
  }

}
