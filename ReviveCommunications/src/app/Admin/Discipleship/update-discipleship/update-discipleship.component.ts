import { Discipleship } from './../../../model/Discipleship';
import { DiscipleshipService } from './../../../Services/discipleship.service';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NbToastrService, NbDialogService } from '@nebular/theme';
import { CancelConfirmDiscipleshipComponent } from '../cancel-confirm-discipleship/cancel-confirm-discipleship.component';

@Component({
  selector: 'app-update-discipleship',
  templateUrl: './update-discipleship.component.html',
  styleUrls: ['./update-discipleship.component.scss']
})
export class UpdateDiscipleshipComponent implements OnInit  {

  DiscipleshipID;
  DiscipleshipDescription;
  disc: any;
  updatedDiscipleship;
  duration = 3000;
  constructor(
    private DiscService: DiscipleshipService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private toastrService: NbToastrService,
    private dialogService: NbDialogService


  ) {

   }

   ngOnInit() {

    this.DiscipleshipID = this.route.snapshot.paramMap.get('id');
    this.DiscService.DiscipleshipByID(this.DiscipleshipID).subscribe(x =>
      this.disc = x,
      );


  }



  onSubmit(disc){
    console.warn(disc);
        this.DiscService.updateDiscipleship(disc);
        this.presentToast(this.duration);

  }

  ValidationError(position, status) {

    this.toastrService.show(
      status || 'Success',
      `Some data is missing or in the wrong format, please make sure all fields are completed and in the correct format.`,
      { position, status});
  }
  async presentToast(duration) {

    this.toastrService.show(
      status || 'Success',
      'Discipleship updated successfully.',
      { duration });
    }

    open() {
      this.dialogService.open(CancelConfirmDiscipleshipComponent, {
        context: {

        },
      });
    }

}
