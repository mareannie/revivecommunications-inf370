import { Discipleship } from '../../../model/Discipleship';
import { Component, OnInit } from '@angular/core';
import { DiscipleshipService } from 'src/app/Services/discipleship.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NbToastrService, NbDialogService } from '@nebular/theme';
import { ConfirmDeleteDialogComponent } from '../confirm-delete-dialog/confirm-delete-dialog.component';


@Component({
  selector: 'app-search-discipleship',
  templateUrl: './search-discipleship.component.html',
  styleUrls: ['./search-discipleship.component.scss']
})
export class SearchDiscipleshipComponent implements OnInit {

  searchText;
  DiscList;
  selected;
    constructor(
    private DiscService :DiscipleshipService,
    private router: Router,
    private route: ActivatedRoute,
    private toastrService: NbToastrService,
    private dialogService: NbDialogService
  ) {
  }

 ngOnInit(): void {
   this.DiscList = this.DiscService.getAllDiscipleships();
   console.log(this.DiscList);
 }


 onDelete(o: Discipleship) {
  console.log(o);
  this.dialogService.open(ConfirmDeleteDialogComponent, {
    context: {
    },
  });

  this.selected = o;
  this.confirmed(o)

 }

 confirmed(o: Discipleship){
   console.log("After delete confirm")
   this.DiscService.deleteDiscipleship(o).subscribe(x=> {
    console.log(x);
    location.reload();
  });

 }


}

