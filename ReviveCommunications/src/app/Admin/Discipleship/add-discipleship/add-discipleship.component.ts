import { DiscipleshipService } from './../../../Services/discipleship.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NbToastrService, NbDialogService } from '@nebular/theme';
import { Router, ActivatedRoute } from '@angular/router';
import { CancelConfirmDiscipleshipComponent } from 'src/app/Admin/Discipleship/cancel-confirm-discipleship/cancel-confirm-discipleship.component';

@Component({
  selector: 'app-add-discipleship',
  templateUrl: './add-discipleship.component.html',
  styleUrls: ['./add-discipleship.component.scss']
})
export class AddDiscipleshipComponent implements OnInit {

  DiscipleshipID;
  DiscipleshipDescription;
  disc: any;
  createdDiscipleship;
  duration = 3000;
  constructor(
    private DiscService: DiscipleshipService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private toastrService: NbToastrService,
    private dialogService: NbDialogService

    )
     {

  }

  ngOnInit() {

  }


  onSubmit(Disc){
    this.DiscService.createDiscipleship(Disc);
    console.log(Disc);
    this.presentToast(this.duration);
   }
   async presentToast(duration) {

    this.toastrService.show(
      status || 'Success',
      'Discipleship Created successfully.',
      { duration });
    }

  SuccessMessage(position, status) {

    this.toastrService.show(
      status || 'Success',
      `New Organisational structure position was added successfully`,
      { position, status});
  }

  ValidationError(position, status) {

    this.toastrService.show(
      status || 'Success',
      `Some data is missing or in the wrong format, please make sure all fields are completed and in the correct format.`,
      { position, status});
  }

    open() {
      this.dialogService.open(CancelConfirmDiscipleshipComponent, {
        context: {

        },
      });
    }
}
