import { OrgIndivPos } from './../../../model/OrgIndivPos';
import { OrgIndivPosService } from './../../../Services/org-indiv-pos.service';
import { Component, OnInit } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { ConfirmDeleteOIPDialogComponent } from '../confirm-delete-oipdialog/confirm-delete-oipdialog.component';


@Component({
  selector: 'app-view-org-indiv-pos',
  templateUrl: './view-org-indiv-pos.component.html',
  styleUrls: ['./view-org-indiv-pos.component.scss']
})
export class ViewOrgIndivPosComponent implements OnInit {

  OrgIndivPosList;
  Usecases;
  Goals;
  selected;
  constructor(
    private OIPService : OrgIndivPosService,
    private dialogService: NbDialogService
  ) { }

  ngOnInit() {

      this.OIPService.getAllOrgIndivPos().subscribe(x =>{
        this.OrgIndivPosList = x
        console.log(this.OrgIndivPosList)}

        );
    }

  onDelete(o: OrgIndivPos) {
   console.log(o);

   this.dialogService.open(ConfirmDeleteOIPDialogComponent, {
    context: {
    },
  });

  this.selected = o;
  this.confirmed(o)
  }

 confirmed(o: OrgIndivPos){
  console.log("After delete confirm")
  this.OIPService.deleteOrgIndivPos(o).subscribe(x=> {
    console.log(x);
  });
   location.reload();


}


}
