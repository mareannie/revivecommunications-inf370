import { OrgIndivPosService } from './../../../Services/org-indiv-pos.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, ValidatorFn, FormArray, FormControl, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NbToastrService, NbDialogService } from '@nebular/theme';
import { CancelConfirmOIPComponent } from '../cancel-confirm-oip/cancel-confirm-oip.component';
import { OrgIndivPos } from 'src/app/model/OrgIndivPos';

@Component({
  selector: 'app-add-org-indiv-pos',
  templateUrl: './add-org-indiv-pos.component.html',
  styleUrls: ['./add-org-indiv-pos.component.scss']
})
export class AddOrgIndivPosComponent implements OnInit {


  OrgIndivPosID;
  oip;
  Created: OrgIndivPos;
  Usecases;
  Goals;
  Positions: OrgIndivPos[];
  createdOrgIndivPos;
  duration = 3000;

  IndivPos;
  Use_Cases = [];
  Goal_Access =[];

  form: FormGroup;
  //

  constructor(
    private OipService: OrgIndivPosService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private toastrService: NbToastrService,
    private dialogService: NbDialogService,
    private fb: FormBuilder

  ) {

  }


  changeSelection() {
    this.fetchSelectedUC()
    this.fetchSelectedGoals()
  }

  fetchSelectedUC() {
    this.Use_Cases = this.Usecases.filter((value, index) => {
      return value.isChecked
    });
  }

  fetchSelectedGoals() {
    this.Goal_Access = this.Goals.filter((value, index) => {
      return value.isChecked
    });
  }



   ngOnInit() {

    this.OrgIndivPosID = this.route.snapshot.paramMap.get('id');
    this.OipService.GetGoalsList().subscribe( x=>
      this.Goals = x);

    this.OipService.GetPositionName().subscribe( x =>
      this.Positions = x)

    this.OipService.GetUseCasesList().subscribe(x =>
      this.Usecases = x);


  }




  onSubmit(oip){
    console.log(oip)

    this.OipService.createOrgIndivPos(oip);
    this.presentToast(this.duration);


  };

  async presentToast(duration) {

    this.toastrService.show(
      'Success',
      'Organisaion Individual Position created successfully.',
      { duration });
    }

    open() {
      this.dialogService.open(CancelConfirmOIPComponent, {
        context: {

        },
      });
    }

}
