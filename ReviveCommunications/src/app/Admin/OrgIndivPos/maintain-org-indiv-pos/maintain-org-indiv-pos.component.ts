import { OrgIndivPosService } from './../../../Services/org-indiv-pos.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, ValidatorFn, FormArray, FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NbToastrService, NbDialogService } from '@nebular/theme';
import { of } from 'rxjs';
import { OrgIndivPos } from 'src/app/model/OrgIndivPos';
import { CancelConfirmOIPComponent } from '../cancel-confirm-oip/cancel-confirm-oip.component';

@Component({
  selector: 'app-maintain-org-indiv-pos',
  templateUrl: './maintain-org-indiv-pos.component.html',
  styleUrls: ['./maintain-org-indiv-pos.component.scss']
})
export class MaintainOrgIndivPosComponent implements OnInit {



  OrgIndivPosID;
  oip;
  Created: OrgIndivPos;
  Usecases;
  Goals;
  Positions: OrgIndivPos[];
  createdOrgIndivPos;
  duration = 3000;

  Decription;
  IndivPos;
  Use_Cases = [];
  Goal_Access =[];

  form: FormGroup;
  //

  constructor(
    private OipService: OrgIndivPosService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private toastrService: NbToastrService,
    private dialogService: NbDialogService,
    private fb: FormBuilder

  ) {

  }


  changeSelection() {
    this.fetchSelectedUC()
    this.fetchSelectedGoals()
  }

  fetchSelectedUC() {
    this.Use_Cases = this.Usecases.filter((value, index) => {
      return value.isChecked
    });
  }

  fetchSelectedGoals() {
    this.Goal_Access = this.Goals.filter((value, index) => {
      return value.isChecked
    });
  }



   ngOnInit() {

    this.OrgIndivPosID = this.route.snapshot.paramMap.get('id');

    this.OipService.OrgIndivPosByID(this.OrgIndivPosID).subscribe( x =>
      {
        this.oip = x;
        console.log(this.oip)
      });
    this.OipService.GetGoalsList().subscribe( x=>
      this.Goals = x);

    this.OipService.GetPositionName().subscribe( x =>
      this.Positions = x)

    this.OipService.GetUseCasesList().subscribe(x =>
      this.Usecases = x);


  }




  onSubmit(oip){
    console.log(oip)

    this.OipService.updateOrgIndivPos(oip);
    this.presentToast(this.duration);


  };

  async presentToast(duration) {

    this.toastrService.show(
      'Success',
      'Organisaion Individual Position created successfully.',
      { duration });
    }

    open() {
      this.dialogService.open(CancelConfirmOIPComponent, {
        context: {

        },
      });
    }

}
