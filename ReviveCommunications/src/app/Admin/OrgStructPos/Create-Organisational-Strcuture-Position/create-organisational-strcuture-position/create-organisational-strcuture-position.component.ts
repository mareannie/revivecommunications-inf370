import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { OrganisationalStructurePositionService } from '../../organisational-structure-position.service';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { CancelConfirmationDialogComponent } from '../../cancel-confirmation-dialog/cancel-confirmation-dialog.component';

@Component({
  selector: 'app-create-organisational-strcuture-position',
  templateUrl: './create-organisational-strcuture-position.component.html',
  styleUrls: ['./create-organisational-strcuture-position.component.scss']
})
export class CreateOrganisationalStrcuturePositionComponent implements OnInit {

  OrgStructPos;

  constructor(private OrgStructPosService: OrganisationalStructurePositionService,
    private toastrService: NbToastrService ,private dialogService: NbDialogService,private formBuilder: FormBuilder) { 
    this.OrgStructPos = this.formBuilder.group({OrgStructLevel:'',
    OrgStructTypeID:'',
    OrgIndivPosID:'',
    OrgStructIDReportTo:'',
    Description:''});
  }

  ngOnInit(): void {
  }

  
  open() {
    this.dialogService.open(CancelConfirmationDialogComponent, {
      context: {

      },
    });
  }

  SuccessMessage(position, status) {

    this.toastrService.show(
      status || 'Success',
      `New Organisational structure position was added successfully`,
      { position, status});
  }
  
  ValidationError(position, status) {

    this.toastrService.show(
      status || 'Success',
      `Some data is missing or in the wrong format, please make sure all fields are completed and in the correct format.`,
      { position, status});
  }
  addOrgStructPosForm(form) {
    
    if(this.OrgStructPos.OrgStructLevel == 1)
    {
      console.log("form");
    }
    else{
      console.log(form);
    }
    
   this.OrgStructPosService.AddOrganisationalStructurePosition(form);
  }
}
