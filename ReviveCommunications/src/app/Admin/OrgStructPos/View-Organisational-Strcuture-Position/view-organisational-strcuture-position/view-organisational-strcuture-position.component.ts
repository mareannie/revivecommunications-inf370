import { Component, OnInit } from '@angular/core';
import { OrgData } from 'angular-org-chart/src/app/modules/org-chart/orgData';

@Component({
  selector: 'app-view-organisational-strcuture-position',
  templateUrl: './view-organisational-strcuture-position.component.html',
  styleUrls: ['./view-organisational-strcuture-position.component.scss']
})
export class ViewOrganisationalStrcuturePositionComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  orgData: OrgData= {
    name: "Ps. At Boshoff",
    type: 'Senior Pastor',
    children: [
        {
            name: "Jack Smith",
            type: 'Zone Pastor',
            children: [
                {
                    name: "Kevin Wilson",
                    type: 'Overseer',
                    children: []
                },
                {
                    name: "Andrea Nel",
                    type: 'Overseer',
                    children: []
                }
            ]
        },
        {
            name: "Jane Muller",
            type: 'Zone Pastor',
            children: [
                {
                    name: "Ndlali Ledwaba",
                    type: 'Overseer',
                    children: [
                        {
                            name: "Jackson Seyfried",
                            type: 'Supervisor',
                            children: []
                        }
                    ]
                },
                {
                    name: "Amanda Schwarts",
                    type: 'Overseer',
                    children: [
                        {
                            name: "Louis Botha",
                            type: 'Supervisor',
                            children: []
                        }
                    ]
                }
            ]
        }
    ]
  };
}
