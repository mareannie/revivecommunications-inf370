import { Component, OnInit } from '@angular/core';
import { NbSidebarService, NbMenuItem, NbToastrService, NbDialogService } from '@nebular/theme';
import { LeaderOption } from 'src/app/model/leaderoptions';
import { ViewChildDialogComponent } from 'src/app/KidsChurch/view-child-dialog/view-child-dialog.component';
import { SignedOutChildDialogComponent } from 'src/app/KidsChurch/signed-out-child-dialog/signed-out-child-dialog.component';

@Component({
  selector: 'app-volunteer-children',
  templateUrl: './volunteer-children.component.html',
  styleUrls: ['./volunteer-children.component.scss']
})
export class VolunteerChildrenComponent implements OnInit {
  user = [
    { title: 'Profile' },
    { title: 'Logout' },
  ];

  constructor(private sidebarService: NbSidebarService, private toastrService: NbToastrService,
    private dialogService: NbDialogService) { }

  open2() {
      this.dialogService.open(SignedOutChildDialogComponent, {
        context: {
  
        },
      });
    }
  
    open3() {
      this.dialogService.open(ViewChildDialogComponent, {
        context: {
  
        },
      });
    }

    ngOnInit(): void {
    }
  
    toggle() {
      this.sidebarService.toggle(true);
      return false;
    }
  
  }
