import { Component, OnInit } from '@angular/core';
import { NbSidebarService, NbMenuItem, NbToastrService } from '@nebular/theme';
import { FormBuilder, FormGroup } from '@angular/forms';
import { KidsChurchService } from 'src/app/Services/kids-church.service'; 
import { IKidsChurch } from 'src/app/model/kidschurch';

export interface tempChild {
  ChildID: number;
  ChildName: string;
  ChildSurname: string;
  DateOfBirth: string;
}

@Component({
  selector: 'app-add-child',
  templateUrl: './add-child.component.html',
  styleUrls: ['./add-child.component.scss']
})
export class AddChildComponent implements OnInit {

  addChild;
  kidschurches: IKidsChurch[];

  checked = false;

  constructor(private sidebarService: NbSidebarService,
              private toastrService: NbToastrService,
              private formbuilder: FormBuilder,
              private kidsChurchService: KidsChurchService) 
              {
                this.addChild = this.formbuilder.group(
                  {
                    ChildName: '',
                    ChildSurname: '',
                    DateOfBirth: '',
                    KidsChurchID: ''
                  }
                );
              }


  ngOnInit() 
  {
    this.kidsChurchService.getKidsChurchClasses()
    .subscribe
    (
      data => 
      {
        this.kidschurches = data;
        console.log(data)
      }
    )
  }

  showToast(position, status) {

    if (this.checked = true)
    {
      this.toastrService.show(
        status || 'Success',
        `Child successfully added`,
        { position, status});
    }
    else
    {
      this.toastrService.show(
        status || 'Danger',
        `Not all field were completed. Please complete all required fields`,
        { position, status});
    }

    
  }

  showErrorToast(position, status) {

    this.toastrService.show(
      status || 'Danger',
      `Not all field were completed. Please complete all required fields`,
      { position, status});
  }

  onSubmit(childName: string, childSurname: string, dateOfBirth: string, event: Event)
  {
    event.preventDefault()
    var childData = {ChildName: childName, ChildSurname: childSurname, DateOfBirth: dateOfBirth} as tempChild;
    this.kidsChurchService.addChild(childData)
  }

}

