import { Component, OnInit } from '@angular/core';
import { NbSidebarService, NbMenuItem, NbToastrService, NbDialogService } from '@nebular/theme';
import { LeaderOption } from 'src/app/model/leaderoptions';
import { ViewChildDialogComponent } from 'src/app/KidsChurch/view-child-dialog/view-child-dialog.component';
import { SignOutChildDialogComponent } from 'src/app/KidsChurch/sign-out-child-dialog/sign-out-child-dialog.component';

@Component({
  selector: 'app-children',
  templateUrl: './children.component.html',
  styleUrls: ['./children.component.scss']
})
export class ChildrenComponent implements OnInit {

  user = [
    { title: 'Profile' },
    { title: 'Logout' },
  ];

  checked = false;

  constructor(private sidebarService: NbSidebarService, private toastrService: NbToastrService,
    private dialogService: NbDialogService) { }

  open() {
    this.dialogService.open(SignOutChildDialogComponent, {
      context: {

      },
    });
  }

  open2() {
    this.dialogService.open(ViewChildDialogComponent, {
      context: {

      },
    });
  }

  loading = false;

  toggleLoadingAnimation() {
    this.loading = true;
    setTimeout(() => this.loading = false, 3000);
  }


  ngOnInit(): void {
  }

  toggle() {
    this.sidebarService.toggle(true);
    return false;
  }

  showToast(position, status) {

    if (this.checked = true)
    {
      this.toastrService.show(
        status || 'Success',
        `Member successfully approved.`,
        { position, status});
    }
    else
    {
      this.toastrService.show(
        status || 'Danger',
        `Member not approved.`,
        { position, status});
    }

    
  }

  showErrorToast(position, status) {

    this.toastrService.show(
      status || 'Danger',
      `Child not found`,
      { position, status});
  }

}