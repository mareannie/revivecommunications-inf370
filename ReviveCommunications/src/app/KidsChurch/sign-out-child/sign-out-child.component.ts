import { Component, OnInit } from '@angular/core';
import { NbSidebarService, NbMenuItem, NbToastrService, NbDialogService } from '@nebular/theme';
import { SignOutChildDialogComponent } from 'src/app/KidsChurch/sign-out-child-dialog/sign-out-child-dialog.component';
import { SignedOutChildDialogComponent } from 'src/app/KidsChurch/signed-out-child-dialog/signed-out-child-dialog.component';

@Component({
  selector: 'app-sign-out-child',
  templateUrl: './sign-out-child.component.html',
  styleUrls: ['./sign-out-child.component.scss']
})
export class SignOutChildComponent implements OnInit {
  user = [
    { title: 'Profile' },
    { title: 'Logout' },
  ];

  constructor(private sidebarService: NbSidebarService, private toastrService: NbToastrService,
    private dialogService: NbDialogService) { }
  
    open() {
      this.dialogService.open(SignOutChildDialogComponent, {
        context: {
  
        },
      });
    }

    open2() {
      this.dialogService.open(SignedOutChildDialogComponent, {
        context: {
  
        },
      });
    }

    loading = false;

  toggleLoadingAnimation() {
    this.loading = true;
    setTimeout(() => this.loading = false, 3000);
  }

  ngOnInit(): void {
  }

  toggle() {
    this.sidebarService.toggle(true);
    return false;
  }

}
