import { Component, OnInit } from '@angular/core';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'app-register-child',
  templateUrl: './register-child.component.html',
  styleUrls: ['./register-child.component.scss']
})
export class RegisterChildComponent implements OnInit {

  constructor( private toastrService: NbToastrService) { }

  ngOnInit(): void {
  }

  classes = [
    { value: 'ClassID 1', label: 'Gr 0' },
    { value: 'ClassID 2', label: 'Gr 1' },
    { value: 'ClassID 3', label: 'Gr 2' },
    { value: 'ClassID 4', label: 'Gr 3' },
  ];
    options = [
      { value: 'ChildID 1', label: 'Freddy Kruger' },
      { value: 'ChildID 2', label: 'Dana Shore' },
      { value: 'ChildID 3', label: 'John Snow' },
      { value: 'ChildID 4', label: 'Frodo Bagins' },
    ];
    option;

  checked = false;

  select(checked: boolean) {
    this.checked = checked;
  }

  onSubmit(registerInfo){
    console.log(registerInfo)

  }

  showToast(position, status) {

    this.toastrService.show(
      status || 'Success',
      `Registration Successful`,
      { position, status});
  }

  Groups: { name: string}[] = [{name:'Child 1'} , {name:'Child 2'}, {name:'Child 3'} ];
  Persons = [ "Gr 1", "Gr 2", "Gr 3" ];
  onClick(){

  }
}
