import { Component, OnInit } from '@angular/core';
import { NbSidebarService, NbMenuItem, NbToastrService } from '@nebular/theme';
import { FormBuilder, FormGroup } from '@angular/forms';
import { KidsChurchService } from 'src/app/Services/kids-church.service'; 
import { IKidsChurch } from 'src/app/model/kidschurch';

export interface tempChild {
  ChildID: number;
  ChildName: string;
  ChildSurname: string;
  DateOfBirth: string;
}

@Component({
  selector: 'app-update-child',
  templateUrl: './update-child.component.html',
  styleUrls: ['./update-child.component.scss']
})
export class UpdateChildComponent implements OnInit {

  updateChild;
  kidschurches: IKidsChurch[];
  child: any;

  checked = false;

  constructor(private sidebarService: NbSidebarService,
              private toastrService: NbToastrService,
              private formbuilder: FormBuilder,
              private kidsChurchService: KidsChurchService) 
              {
                this.updateChild = this.formbuilder.group(
                  {
                    ChildName: '',
                    ChildSurname: '',
                    DateOfBirth: '',
                    KidsChurchID: ''
                  }
                );
              }

  ngOnInit() 
  {
    this.kidsChurchService.getKidsChurchClasses()
    .subscribe
    (
      data => 
      {
        this.kidschurches = data;
        console.log(data)
      }
    );
    this.child = this.kidsChurchService.getData();
    console.log(this.child);
    if (this.child != null)
    {
      this.updateChild.controls.ChildName.setValue(this.child.ChildName);
      this.updateChild.controls.ChildSurname.setValue(this.child.ChildSurname);
      this.updateChild.controls.DateOfBirth.setValue(this.child.DateOfBirth);
      this.updateChild.controls.KidsChurchID.setValue(this.child.KidsChurchID);      
    }
  }

  toggle() {
    this.sidebarService.toggle(true);
    return false;
  }

  showToast(position, status) {

    if (this.checked = true)
    {
      this.toastrService.show(
        status || 'Success',
        `Child successfully updated.`,
        { position, status});
    }
    else
    {
      this.toastrService.show(
        status || 'Danger',
        `Not all field were completed. Please complete all required fields`,
        { position, status});
    }

    
  }

  showErrorToast(position, status) {

    this.toastrService.show(
      status || 'Danger',
      `Not all field were completed. Please complete all required fields`,
      { position, status});
  }

  onSubmit(childName: string, childSurname: string, dateOfBirth: string, event: Event)
  {
    event.preventDefault()
    var childData = {ChildName: childName, ChildSurname: childSurname, DateOfBirth: dateOfBirth} as tempChild;
    this.kidsChurchService.updateChild(childData)
  }

}

