import { Component, OnInit } from '@angular/core';
import { NbSidebarService, NbMenuItem, NbToastrService, NbDialogService } from '@nebular/theme';
import { SignedOutChildDialogComponent } from 'src/app/KidsChurch/signed-out-child-dialog/signed-out-child-dialog.component';

@Component({
  selector: 'app-volunteer-sign-out-approve',
  templateUrl: './volunteer-sign-out-approve.component.html',
  styleUrls: ['./volunteer-sign-out-approve.component.scss']
})
export class VolunteerSignOutApproveComponent implements OnInit {

  user = [
    { title: 'Profile' },
    { title: 'Logout' },
  ];

  constructor(private sidebarService: NbSidebarService, private toastrService: NbToastrService,
    private dialogService: NbDialogService) { }
  
    open2() {
      this.dialogService.open(SignedOutChildDialogComponent, {
        context: {
  
        },
      });
    }

    ngOnInit(): void {
    }
  
    toggle() {
      this.sidebarService.toggle(true);
      return false;
    }
  
    
  
  }
