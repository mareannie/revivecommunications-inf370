import { Component, OnInit } from '@angular/core';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'app-overview-structure-report',
  templateUrl: './overview-structure-report.component.html',
  styleUrls: ['./overview-structure-report.component.scss']
})
export class OverviewStructureReportComponent implements OnInit {

  constructor(private toastrService: NbToastrService,) { }

  ngOnInit(): void {
  }

  filterEnable : boolean;
  DeleteEnable: boolean;

  filter()
  {
    if(this.filterEnable == true)
    {
      this.filterEnable = false;
    }
    else
    {
      this.filterEnable = true;
    }

  }
}
