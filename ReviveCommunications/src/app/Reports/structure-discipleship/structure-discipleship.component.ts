import { Component, OnInit } from '@angular/core';
import { NbSidebarService, NbToastrService, NbMenuItem } from '@nebular/theme';

@Component({
  selector: 'app-structure-discipleship',
  templateUrl: './structure-discipleship.component.html',
  styleUrls: ['./structure-discipleship.component.scss']
})
export class StructureDiscipleshipComponent implements OnInit {

  user = [
    { title: 'Profile' },
    { title: 'Logout' },
  ];

  filterEnable : boolean;
  DeleteEnable: boolean;
  constructor(
    private sidebarService: NbSidebarService,
    private toastrService: NbToastrService,
  ) { }

   //Dummy Data

   Groups: { name: string, type : string }[] = [{name:'Group1', type: 'Homecell'} , {name:'Group2', type: 'Volenteer'}, {name:'Group3', type: 'Kids Church'} ];
   Persons = [ "Person 1", "Person 2", "Person 3" ];
 
   onClick(){
 
   }
   toggle() {
     this.sidebarService.toggle(true);
     return false;
   }
 
   checked = false;
 
   select(checked: boolean) {
     this.checked = checked;
   }
 
   showToast(position, status) {
 
     this.toastrService.show(
       status || 'Success',
       `Message was successfully sent.`,
       { position, status});}
  
 
   ngOnInit() {
    
   }
   
   filter()
   {
     if(this.filterEnable == true)
     {
       this.filterEnable = false;
     }
     else
     {
       this.filterEnable = true;
     }
     
   }
 
 }
