import { Component, OnInit } from '@angular/core';
import { NbSidebarService, NbMenuItem, NbToastrService } from '@nebular/theme';

@Component({
  selector: 'app-zone-church-attendance-feedback',
  templateUrl: './zone-church-attendance-feedback.component.html',
  styleUrls: ['./zone-church-attendance-feedback.component.scss']
})
export class ZoneChurchAttendanceFeedbackComponent implements OnInit {

  show: boolean;

  user = [
    { title: 'Profile' },
    { title: 'Logout' },
  ];

  constructor(private sidebarService: NbSidebarService, private toastrService: NbToastrService) { }

  settings = {
    selectMode: 'multi',
    hideSubHeader: true,
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
    columns: {
      name: {
        title: 'Group Name',  
      },
    },
  };


  ngOnInit(): void {
  }

  toggle() {
    this.sidebarService.toggle(true);
    return false;
  }

  checked = false;

  select(checked: boolean) {
    this.checked = checked;
  }

  open() {
    this.show = true;
  }

  showToast(position, status) {

    this.toastrService.show(
      status || 'Success',
      `Zone Church Attendance Goal Feedback successfully saved.`,
      { position, status});}

}
