import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZoneChurchAttendanceFeedbackComponent } from './zone-church-attendance-feedback.component';

describe('ZoneChurchAttendanceFeedbackComponent', () => {
  let component: ZoneChurchAttendanceFeedbackComponent;
  let fixture: ComponentFixture<ZoneChurchAttendanceFeedbackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZoneChurchAttendanceFeedbackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoneChurchAttendanceFeedbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
