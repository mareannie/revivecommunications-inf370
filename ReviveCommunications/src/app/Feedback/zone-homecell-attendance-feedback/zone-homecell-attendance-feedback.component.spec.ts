import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZoneHomecellAttendanceFeedbackComponent } from './zone-homecell-attendance-feedback.component';

describe('ZoneHomecellAttendanceFeedbackComponent', () => {
  let component: ZoneHomecellAttendanceFeedbackComponent;
  let fixture: ComponentFixture<ZoneHomecellAttendanceFeedbackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZoneHomecellAttendanceFeedbackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoneHomecellAttendanceFeedbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
