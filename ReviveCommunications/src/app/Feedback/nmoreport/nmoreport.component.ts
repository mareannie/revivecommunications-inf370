import { Component, OnInit } from '@angular/core';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'app-nmoreport',
  templateUrl: './nmoreport.component.html',
  styleUrls: ['./nmoreport.component.scss']
})
export class NMOReportComponent implements OnInit {

  show: boolean;
  constructor(private toastrService: NbToastrService) { }

  ngOnInit(): void {
  }

  showToast(position, status) {

    this.toastrService.show(
      status || 'Success',
      `Organisational structure position was created successfully.`,
      { position, status});}

      open() {
        this.show = true;
      }

}
