import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZoneGrowthFeedbackComponent } from './zone-growth-feedback.component';

describe('ZoneGrowthFeedbackComponent', () => {
  let component: ZoneGrowthFeedbackComponent;
  let fixture: ComponentFixture<ZoneGrowthFeedbackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZoneGrowthFeedbackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoneGrowthFeedbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
