import { Component, OnInit } from '@angular/core';
import { NbSidebarService, NbMenuItem, NbToastrService } from '@nebular/theme';

@Component({
  selector: 'app-zone-growth-feedback',
  templateUrl: './zone-growth-feedback.component.html',
  styleUrls: ['./zone-growth-feedback.component.scss']
})
export class ZoneGrowthFeedbackComponent implements OnInit {
  
  show: boolean;

  user = [
    { title: 'Profile' },
    { title: 'Logout' },
  ];

  constructor(private sidebarService: NbSidebarService,private toastrService: NbToastrService) { }

  settings = {
    selectMode: 'multi',
    hideSubHeader: true,
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
    columns: {
      name: {
        title: 'Group Name',  
      },
    },
  };


  ngOnInit(): void {
  }

  toggle() {
    this.sidebarService.toggle(true);
    return false;
  }

  checked = false;

  select(checked: boolean) {
    this.checked = checked;
  }

  open() {
    this.show = true;
  }

  showToast(position, status) {

    this.toastrService.show(
      status || 'Success',
      `Zone Growth Goal Feedback successfully saved.`,
      { position, status});
  }

}