import { Component, OnInit } from '@angular/core';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'app-report-structure-growth',
  templateUrl: './report-structure-growth.component.html',
  styleUrls: ['./report-structure-growth.component.scss']
})
export class ReportStructureGrowthComponent implements OnInit {

  show: boolean;
  constructor(private toastrService: NbToastrService) { }

  ngOnInit(): void {
  }

  showToast(position, status) {

    this.toastrService.show(
      status || 'Success',
      `Organisational structure position was created successfully.`,
      { position, status});}

      open() {
        this.show = true;
      }

}
