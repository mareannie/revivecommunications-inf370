import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SalvationServiceService {
  constructor( private http: HttpClient) {}

  addNewSalvation(newSalvation)
  {
    this.http.post('https://localhost:44390/api/Salvation/addSalvationInformation', newSalvation).subscribe(x => {
      console.log(x)
    })
  }

  Salvation(): Observable<any>{
    return this.http.get('https://localhost:44390/api/FollowUp/Salvation');
  }

}

