import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class FollowUpDiscService {

  private SERVER_URL = 'https://localhost:44390/';
  constructor(private http: HttpClient,
    private router: Router) { }

    getFollowUpList(){
      return this.http.get<any[]>(this.SERVER_URL + "api/FollowUp/getIncompleteDiscipleshipMembers");
    }

    getMemberDiscData(){
      return this.http.get<any[]>(this.SERVER_URL + "api/FollowUp/getMemberDiscData");
    }


}
