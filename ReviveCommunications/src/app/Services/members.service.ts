import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class MembersService {

  member;

  constructor(private http: HttpClient) { }

  getMembers(): Observable<any>
  {
    return this.http.get('https://localhost:44390/api/Members/getAllMembers')
  }

  getUnapprovedMembers(): Observable<any>
  {
    return this.http.get('https://localhost:44390/api/Members/getAllUnapprovedMembers')
  }

  updateActivationStatus(activationStatus)
  {
    this.http.post('https://localhost:44390/api/Members/updateMemberActivationStatus', activationStatus).subscribe(d => {
      console.log(d)
    });
  }

}
