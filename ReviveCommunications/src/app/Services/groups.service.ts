import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class GroupsService {

  group;

  constructor(private http: HttpClient) { }

  getGroups(): Observable<any>
  {
    return this.http.get('https://localhost:44390/api/Groups/getAllGroups')
  }

  addGroup(newGroup)
  {
    this.http.post('https://localhost:44390/api/Groups/addGroup', newGroup).subscribe(c => {
      console.log(c)
    });
  }

  updateGroup(updateGroup)
  {
    this.http.post('https://localhost:44390/api/Groups/updateGroup', updateGroup).subscribe(c => {
      console.log(c)
    });
  }

  setData(orggroup)
  {
    console.log(orggroup);
    this.group = orggroup;
  }

  getData()
  {
    console.log(this.group);
    return this.group;
  }
}
