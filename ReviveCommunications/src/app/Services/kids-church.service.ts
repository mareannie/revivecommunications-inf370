import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class KidsChurchService {

  child;

  constructor( private http: HttpClient) { }

  getChildren(): Observable<any>
  {
    return this.http.get('https://localhost:44390/api/KidsChurch/getAllChildren')
  }

  getKidsChurchClasses(): Observable<any>
  {
    return this.http.get('https://localhost:44390/api/KidsChurch/getKidsChurch')
  }

  getParentChildren(): Observable<any>
  {
    return this.http.get('https://localhost:44390/api/KidsChurch/getAllPersonChildren')
  }

  addChild(newChild)
  {
    this.http.post('https://localhost:44390/api/KidsChurch/addChild', newChild).subscribe(c => {
      console.log(c)
    });
  }

  updateChild(updateChild)
  {
    this.http.post('https://localhost:44390/api/KidsChurch/addChild', updateChild).subscribe(cc => {
      console.log(cc)
    });
  }

  setData(children)
  {
    console.log(children);
    this.child = children;
  }

  getData()
  {
    console.log(this.child);
    return this.child;
  }
}
