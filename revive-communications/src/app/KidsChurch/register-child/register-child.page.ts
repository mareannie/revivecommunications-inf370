import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register-child',
  templateUrl: './register-child.page.html',
  styleUrls: ['./register-child.page.scss'],
})
export class RegisterChildPage implements OnInit {

  Child = [
    {key: '1', text: "Child 1"},
    {key: '2', text: "Child 2"},
    {key: '3', text: "Child 3"}
    ];
      
    
  Class = [
    {key: '1', text: "Class 1"},
    {key: '2', text: "Class 2"},
    {key: '3', text: "Class 3"}
    ];
  constructor() { }

  ngOnInit() {
  }

}
