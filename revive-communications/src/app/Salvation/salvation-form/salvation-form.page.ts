import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-salvation-form',
  templateUrl: './salvation-form.page.html',
  styleUrls: ['./salvation-form.page.scss'],
})
export class SalvationFormPage implements OnInit {
PersonalInformation: boolean;
Education: boolean;
PrayerRequest: boolean;
constructor(public toastController: ToastController) {}

  ngOnInit() {
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'All done! We received your infomration. We really encourage you to visit us again.',
      duration: 5000
    });
    toast.present();
  }
}

