import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-send-invitation',
  templateUrl: './send-invitation.page.html',
  styleUrls: ['./send-invitation.page.scss'],
})
export class SendInvitationPage implements OnInit {

  
  Persons = [
    {key: '1', text: "Person 1"},
    {key: '2', text: "Person 2"},
    {key: '3', text: "Person 3"}
    ];
      
    
  Groups = [
    {key: '1', text: "Group 1"},
    {key: '2', text: "Group 2"},
    {key: '3', text: "Group 3"}
    ];
      
  constructor() { }

  ngOnInit() {
  }

}
