import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-homecell-notes',
  templateUrl: './homecell-notes.page.html',
  styleUrls: ['./homecell-notes.page.scss'],
})
export class HomecellNotesPage implements OnInit {
filter: boolean;
  constructor() { }

  ngOnInit() {
    this.filter = false;
  }

  Filter(){
    if(this.filter == false)
    {
      this.filter = true;
    }
    else{
      this.filter = false;
    }

}}
