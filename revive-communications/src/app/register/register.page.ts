import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  openChild() {
    document.getElementById("Child").style.display = "block";
    document.getElementById("Personal").style.display = "none";
  }
  openSecurity() {
    document.getElementById("Security").style.display = "block";
    document.getElementById("Child").style.display = "none";
  }
  openDone() {
    document.getElementById("Done").style.display = "block";
    document.getElementById("Security").style.display = "none";
  }
  checked : boolean = false;
  openForm(): void {
    document.getElementById("ChildForm").style.display = "block";
    console.log(this.checked);//it is working !!!

  }
}
